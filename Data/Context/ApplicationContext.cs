﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CodeFirst.Models;
using Core.Context;
using Microsoft.AspNet.Identity.EntityFramework;

namespace Data.Context
{
    public class ApplicationContext : IdentityDbContext<ApplicationUser>, IApplicationContext
    {
        public ApplicationContext() : base("MusicStoreV2") { }

        public static ApplicationContext Create()
        {
            return new ApplicationContext();
        }

        public DbSet<TEntity> Set<TEntity>() where TEntity : BaseEntity
        {
            return base.Set<TEntity>();
        }
        public void SetAsAdded<TEntity>(TEntity entity) where TEntity : BaseEntity
        {
            UpdateEntityState(entity, EntityState.Added);
        }

        public void SetAsModified<TEntity>(TEntity entity) where TEntity : BaseEntity
        {
            UpdateEntityState(entity, EntityState.Modified);
        }

        public void SetAsDeleted<TEntity>(TEntity entity) where TEntity : BaseEntity
        {
            UpdateEntityState(entity, EntityState.Deleted);
        }
        private void UpdateEntityState<TEntity>(TEntity entity, EntityState entityState) where TEntity : BaseEntity
        {
            var dbEntityEntry = GetDbEntityEntrySafely(entity);
            dbEntityEntry.State = entityState;
        }

        private DbEntityEntry GetDbEntityEntrySafely<TEntity>(TEntity entity) where TEntity : BaseEntity
        {
            var dbEntityEntry = Entry<TEntity>(entity);
            if (dbEntityEntry.State == EntityState.Detached)
            {
                Set<TEntity>().Attach(entity);
            }
            return dbEntityEntry;
        }

        public System.Data.Entity.DbSet<CodeFirst.Models.Category> Categories { get; set; }

        public System.Data.Entity.DbSet<CodeFirst.Models.Brand> Brands { get; set; }

        public System.Data.Entity.DbSet<CodeFirst.Models.Subcategory> Subcategories { get; set; }
        //public System.Data.Entity.DbSet<CodeFirst.Models.ApplicationUser> ApplicationUsers { get; set; }
    }
}
