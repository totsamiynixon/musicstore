namespace Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class productEntity_defaultImage : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Products", "DefaultImageId", c => c.Int(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.Products", "DefaultImageId");
        }
    }
}
