namespace Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class ApplicationUserChanges : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.AspNetUsers", "ApplicationRole_Id", "dbo.AspNetRoles");
            DropIndex("dbo.AspNetUsers", new[] { "ApplicationRole_Id" });
            DropColumn("dbo.AspNetUsers", "ApplicationRoleId");
            DropColumn("dbo.AspNetUsers", "ApplicationRole_Id");
        }
        
        public override void Down()
        {
            AddColumn("dbo.AspNetUsers", "ApplicationRole_Id", c => c.String(maxLength: 128));
            AddColumn("dbo.AspNetUsers", "ApplicationRoleId", c => c.Int(nullable: false));
            CreateIndex("dbo.AspNetUsers", "ApplicationRole_Id");
            AddForeignKey("dbo.AspNetUsers", "ApplicationRole_Id", "dbo.AspNetRoles", "Id");
        }
    }
}
