﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using CodeFirst.Models;
using Core.Data;
using Core.Services;

namespace Services
{
    public class StockService : IStockService
    {
        private readonly IRepository<Stock> _stockRepository;
        private bool _disposed;

        public StockService(IRepository<Stock> stockRepository)
        {

            _stockRepository = stockRepository;
        }

        public Task<List<Stock>> GetAllStocks()
        {
            return _stockRepository.GetAllAsync();
        }

        public Task<Stock> GetStockById(int? id)
        {
            return _stockRepository.GetSingleIncludingAsync(w => w.Id == id);
        }

        public Task<List<Stock>> GetAllStocksByProductId(int? productId)
        {
            return _stockRepository.GetAllIncludingAsync(s => s.ProductId == productId);
        }

        public Task<List<Stock>> GetAllStocksByWarehouseId(int? warehouseId)
        {
            return _stockRepository.GetAllIncludingAsync(s => s.WarehouseId == warehouseId);
        }

        public async Task<int> AddOrUpdate(Stock stock)
        {
            if (stock == null) throw new ArgumentNullException(nameof(stock));

            var stockFromDB = await _stockRepository.GetSingleIncludingAsync(w => w.Id == stock.Id);
            if (stockFromDB == null)
            {
                _stockRepository.Insert(stock);
            }
            else
            {
                _stockRepository.Update(stock);
            }
            await _stockRepository.SaveChangesAsync();

            return stockFromDB?.Id ?? stock.Id;
        }

        public Task<List<Stock>> GetAllStocksWithProducts()
        {
            //проверить, тащат ли ордеры за собой остальные энтити или нет
            return _stockRepository.GetAllIncludingAsync(o => o.Product);
        }

        public async Task Remove(int id)
        {
            var stock = await _stockRepository.GetSingleAsync(w => w.Id == id);
            _stockRepository.Delete(stock);
            await _stockRepository.SaveChangesAsync();
        }

        public async Task Remove(List<int> ids)
        {
            if (ids == null) throw new ArgumentNullException(nameof(ids));

            var stocks = await _stockRepository.FindByAsync(w => ids.Any(a => a == w.Id));

            foreach (var stock in stocks)
            {
                _stockRepository.Delete(stock);
            }

            await _stockRepository.SaveChangesAsync();
        }
    }
}