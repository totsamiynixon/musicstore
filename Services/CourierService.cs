﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using Core.Services;
using CodeFirst.Models;
using Core.Data;

namespace Services
{
   public class CourierService : ICourierService
    {
        private readonly IRepository<Courier> _courierRepository;
        private bool _disposed;

        public CourierService(IRepository<Courier> courierRepository)
        {

            _courierRepository = courierRepository;
        }

        public Task<List<Courier>> GetAllCouriers()
        {
            return _courierRepository.GetAllAsync();
        }

        public Task<List<Courier>> GetAllCouriersWithOrders()
        {
            //проверить, тащат ли ордеры за собой остальные энтити или нет
            return _courierRepository.GetAllIncludingAsync(o=>o.Orders);
        }

        public Task<Courier> GetCourierWithOrders(int id)
        {
            //проверить, тащат ли ордеры за собой отсальные энтити или нет
            return _courierRepository.GetSingleIncludingAsync(c=>c.Id == id,o => o.Orders);
        }

        public Task<Courier> GetCourierById(int? id)
        {
            return _courierRepository.GetSingleIncludingAsync(w => w.Id == id);
        }

        public async Task<int> AddOrUpdate(Courier courier)
        {
            if (courier == null) throw new ArgumentNullException(nameof(courier));

            var courierFromDB = await _courierRepository.GetSingleIncludingAsync(w => w.Id == courier.Id);
            if (courierFromDB == null)
            {
                _courierRepository.Insert(courier);
            }
            else
            {
                _courierRepository.Update(courier);
            }
            await _courierRepository.SaveChangesAsync();

            return courierFromDB?.Id ?? courier.Id;
        }
        public async Task Remove(int id)
        {
            var courier = await _courierRepository.GetSingleAsync(w => w.Id == id);
            _courierRepository.Delete(courier);
            await _courierRepository.SaveChangesAsync();
        }

        public async Task Remove(List<int> ids)
        {
            if (ids == null) throw new ArgumentNullException(nameof(ids));

            var couriers = await _courierRepository.FindByAsync(w => ids.Any(a => a == w.Id));

            foreach (var courier in couriers)
            {
                _courierRepository.Delete(courier);
            }

            await _courierRepository.SaveChangesAsync();
        }
    }
}
