﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using Core.Data;
using Core.Services;
using Image = CodeFirst.Models.Image;

namespace Services
{
    public class ImageService : IImageService
    {
        private readonly IRepository<Image> _imageRepository;

        public ImageService(
            IRepository<Image> imageRepository)
        {
            _imageRepository = imageRepository;
        }
        public Task<List<Image>> GetAllImagesByProductId(int productId)
        {
            return _imageRepository.FindByAsync(i => i.ProductId == productId);
        }

        public Task<Image> GetImageById(int imageId)
        {
            return _imageRepository.GetSingleAsync(i => i.Id == imageId);
        }
        public async Task<int> UploadProductImage(HttpPostedFileBase hpf, int productId)
        {
            int id = 0;
            var newBMP = ImageResizer(hpf, 0, 0);
            string imageName = hpf.FileName.ToString();
            string filePathWithoutName =
                HttpContext.Current.Server.MapPath(ConfigurationManager.AppSettings["productImagesPath"] +
                                                   "Products/p" + productId + "/");
            string filePath =
                HttpContext.Current.Server.MapPath(ConfigurationManager.AppSettings["productImagesPath"] +
                                                "Products/p" + productId + "/" + imageName);
            SaveImageOnLocalHost(newBMP, filePathWithoutName, filePath);
            Image productImage = new Image
            {
                ProductId = productId,
                //жестко кодируем путь к папке КОСТЫЛЬ!!!!
                RelatedUrl = "/Uploads/Products/p" + productId + "/" + imageName
            };
            _imageRepository.Insert(productImage);
            await _imageRepository.SaveChangesAsync();
            if (id == 0)
            {
                id = productImage.Id;
            }
            return productImage.Id;
        }

        public async Task<int> UploadProductImages(IEnumerable<HttpPostedFileBase> hpfs, int productId)
        {
            Image productImage = new Image();
            int defaultImageId = 0;
            foreach (var hpf in hpfs)
            {
                var newBMP = ImageResizer(hpf, 0, 0);
                string imageName = hpf.FileName.ToString();
                string filePathWithoutName =
                    HttpContext.Current.Server.MapPath(ConfigurationManager.AppSettings["productImagesPath"] +
                                                       "Products/p" + productId + "/");
                string filePath =
                    HttpContext.Current.Server.MapPath(ConfigurationManager.AppSettings["productImagesPath"] +
                                                    "Products/p" + productId + "/" + imageName);
                SaveImageOnLocalHost(newBMP,filePathWithoutName,filePath);
                productImage = new Image
                {
                    ProductId = productId,
                    //жестко кодируем путь к папке КОСТЫЛЬ!!!!
                    RelatedUrl = "/Uploads/Products/p"+ productId +"/" + imageName
                };
                _imageRepository.Insert(productImage);
                await _imageRepository.SaveChangesAsync();
                if (defaultImageId == 0)
                {
                    defaultImageId = productImage.Id;
                }
            }
            return defaultImageId;
        }

        public async Task<List<Image>> UploadProductImagesReturnImages(IEnumerable<HttpPostedFileBase> hpfs, int productId)
        {
            Image productImage = new Image();
            List<Image> images = null;
            int ident = 0;
            foreach (var hpf in hpfs)
            {
                var newBMP = ImageResizer(hpf, 0, 0);
                string imageName = hpf.FileName.ToString();
                string filePathWithoutName =
                    HttpContext.Current.Server.MapPath(ConfigurationManager.AppSettings["productImagesPath"] +
                                                       "Products/p" + productId + "/");
                string filePath =
                    HttpContext.Current.Server.MapPath(ConfigurationManager.AppSettings["productImagesPath"] +
                                                    "Products/p" + productId + "/" + imageName);
                SaveImageOnLocalHost(newBMP, filePathWithoutName, filePath);
                productImage = new Image
                {
                    ProductId = productId,
                    //жестко кодируем путь к папке КОСТЫЛЬ!!!!
                    RelatedUrl = "/Uploads/Products/p" + productId + "/" + imageName
                };
                _imageRepository.Insert(productImage);
                _imageRepository.SaveChanges();
                if (ident == 0)
                {
                    ident++;
                    images = new List<Image>();
                }
                images.Add(productImage);
            }
            return images;
        }

        public async Task RemoveImage(int id)
        {
            var image = await _imageRepository.GetSingleAsync(w => w.Id == id);
  
            string filePath =
                HttpContext.Current.Server.MapPath(image.RelatedUrl);

            if (System.IO.File.Exists(filePath))
                System.IO.File.Delete(filePath);
            _imageRepository.Delete(image);
            await _imageRepository.SaveChangesAsync();

        }
        //РАЗОБРАТЬСЯ c потоками
        public async Task RemoveAllImagesWithProductId(int productId)
        {
            var images = _imageRepository.FindByAsync(i => i.ProductId == productId);
            if (images != null)
            {
                foreach (var image in images.Result)
                {
                    string filePath =
                    HttpContext.Current.Server.MapPath(image.RelatedUrl);
                    if (System.IO.File.Exists(filePath))
                        System.IO.File.Delete(filePath);
                    _imageRepository.Delete(image);
                }
            }
           await _imageRepository.SaveChangesAsync();


        }

        private Bitmap ImageResizer(HttpPostedFileBase hpf,int width,int height)
        {
            Bitmap originalPic = new Bitmap(hpf.InputStream, false);


            Bitmap newBMP = new Bitmap(originalPic);
            Graphics oGraphics = Graphics.FromImage(newBMP);
            oGraphics.SmoothingMode = System.Drawing.Drawing2D.SmoothingMode.AntiAlias;
            oGraphics.InterpolationMode = System.Drawing.Drawing2D.InterpolationMode.HighQualityBicubic;
            oGraphics.DrawImage(originalPic, width, height);
            return newBMP;
        }

        private void SaveImageOnLocalHost(Bitmap newBMP,string filePathWithoutName, string filePath)
        {
            if (!System.IO.Directory.Exists(filePathWithoutName))
            {
                System.IO.Directory.CreateDirectory(filePathWithoutName);
            }
            if (System.IO.File.Exists(filePath))
                System.IO.File.Delete(filePath);
            newBMP.Save(filePath);
        }
    }
}