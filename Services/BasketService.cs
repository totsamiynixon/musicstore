﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CodeFirst.Models;
using Core.Data;
using Core.Services;

namespace Services
{
   public class BasketService : IBasketService
    {

        private readonly IRepository<Basket> _basketRepository;
        private bool _disposed;

        public BasketService(IRepository<Basket> basketRepository)
        {

            _basketRepository = basketRepository;
        }

        public Task<List<Basket>> GetAllBaskets()
        {
            return _basketRepository.GetAllAsync();
        }

        public Task<Basket> GetBasketById(int? id)
        {
            return _basketRepository.GetSingleAsync(w => w.Id == id);
        }

        public async Task<int> AddOrUpdate(Basket basket)
        {
            if (basket == null) throw new ArgumentNullException(nameof(basket));

            var basketFromDB = await _basketRepository.GetSingleIncludingAsync(w => w.Id == basket.Id);
            if (basketFromDB == null)
            {
                _basketRepository.Insert(basket);
            }
            else
            {
                _basketRepository.Update(basket);
            }
            await _basketRepository.SaveChangesAsync();

            return basketFromDB?.Id ?? basket.Id;
        }
        public async Task Remove(int id)
        {
            var basket = await _basketRepository.GetSingleAsync(w => w.Id == id);
            _basketRepository.Delete(basket);
            await _basketRepository.SaveChangesAsync();
        }

        public async Task Remove(List<int> ids)
        {
            if (ids == null) throw new ArgumentNullException(nameof(ids));

            var baskets = await _basketRepository.FindByAsync(w => ids.Any(a => a == w.Id));

            foreach (var basket in baskets)
            {
                _basketRepository.Delete(basket);
            }

            await _basketRepository.SaveChangesAsync();
        }

    }
}
