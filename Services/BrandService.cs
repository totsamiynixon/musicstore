﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using CodeFirst.Models;
using Core.Data;
using Core.Services;

namespace Services
{
    public class BrandService : IBrandService
    {

        private readonly IRepository<Brand> _brandRepository;
        private bool _disposed;

        public BrandService(IRepository<Brand> brandRepository)
        {

            _brandRepository = brandRepository;
        }

        public Task<List<Brand>> GetAllBrands()
        {
            return _brandRepository.GetAllAsync();
        }
        public Task<List<Brand>> FindBrandsBy(Expression<Func<Brand, bool>> predicate)
        {
            return _brandRepository.FindByAsync(predicate);
        }
        public Task<List<Brand>> GetBrandsByCategoryId(int categoryId)
        {
            return _brandRepository.FindByAsync(m=>m.Products.FindAll(p=>p.Subcategory.CategoryId == categoryId).Count>0);
        }
        public Task<List<Brand>> GetBrandsBySubcategoryId(int subcategoryId)
        {
            return _brandRepository.FindByAsync(m => m.Products.Where(p => p.SubcategoryId ==subcategoryId) != null);
        }

        public Task<Brand> GetBrandById(int? id)
        {
            return _brandRepository.GetSingleIncludingAsync(w => w.Id == id);
        }

        public async Task<int> AddOrUpdate(Brand brand)
        {
            if (brand == null) throw new ArgumentNullException(nameof(brand));

            var brandFromDB = await _brandRepository.GetSingleIncludingAsync(w => w.Id == brand.Id);
            if (brandFromDB == null)
            {
                _brandRepository.Insert(brand);
            }
            else
            {
                _brandRepository.Update(brand);
            }
            await _brandRepository.SaveChangesAsync();

            return brandFromDB?.Id ?? brand.Id;
        }
        public async Task Remove(int id)
        {
            var brand = await _brandRepository.GetSingleAsync(w => w.Id == id);
            _brandRepository.Delete(brand);
            await _brandRepository.SaveChangesAsync();
        }

        public async Task Remove(List<int> ids)
        {
            if (ids == null) throw new ArgumentNullException(nameof(ids));

            var brands = await _brandRepository.FindByAsync(w => ids.Any(a => a == w.Id));

            foreach (var brand in brands)
            {
                _brandRepository.Delete(brand);
            }

            await _brandRepository.SaveChangesAsync();
        }
    }
}
