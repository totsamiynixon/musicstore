﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CodeFirst.Models;
using Core.Data;
using Core.Services;

namespace Services
{
    public class OrderService : IOrderService
    {
        private readonly IRepository<Order> _orderRepository;
        private bool _disposed;

        public OrderService(IRepository<Order> orderRepository)
        {

            _orderRepository = orderRepository;
        }

        public Task<List<Order>> GetAllOrders()
        {
            return _orderRepository.GetAllAsync();
        }



        public Task<Order> GetOrderById(int? id)
        {
            return _orderRepository.GetSingleIncludingAsync(w => w.Id == id);
        }

        public async Task<int> AddOrUpdate(Order order)
        {
            if (order == null) throw new ArgumentNullException(nameof(order));

            var orderFromDB = await _orderRepository.GetSingleIncludingAsync(w => w.Id == order.Id);
            if (orderFromDB == null)
            {
                _orderRepository.Insert(order);
            }
            else
            {
                _orderRepository.Update(order);
            }
            await _orderRepository.SaveChangesAsync();

            return orderFromDB?.Id ?? order.Id;
        }
        public async Task Remove(int id)
        {
            var order = await _orderRepository.GetSingleAsync(w => w.Id == id);
            _orderRepository.Delete(order);
            await _orderRepository.SaveChangesAsync();
        }

        public async Task Remove(List<int> ids)
        {
            if (ids == null) throw new ArgumentNullException(nameof(ids));

            var orders = await _orderRepository.FindByAsync(w => ids.Any(a => a == w.Id));

            foreach (var order in orders)
            {
                _orderRepository.Delete(order);
            }

            await _orderRepository.SaveChangesAsync();
        }
    }
}
