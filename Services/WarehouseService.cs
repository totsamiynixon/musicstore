﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CodeFirst.Models;
using Core.Data;
using Core.Services;

namespace Services
{
    public class WarehouseService : IWarehouseService
    {
        private readonly IRepository<Warehouse> _warehouseRepository;
        private bool _disposed;

        public WarehouseService(IRepository<Warehouse> warehouseRepository)
        {

            _warehouseRepository = warehouseRepository;
        }

        public Task<List<Warehouse>> GetAllWarehouses()
        {
            return _warehouseRepository.GetAllAsync();
        }

        public Task<Warehouse> GetWarehouseById(int? id)
        {
            return _warehouseRepository.GetSingleIncludingAsync(w => w.Id == id);
        }

        public async Task<int> AddOrUpdate(Warehouse warehouse)
        {
            if (warehouse == null) throw new ArgumentNullException(nameof(warehouse));

            var warehouseFromDB = await _warehouseRepository.GetSingleIncludingAsync(w => w.Id == warehouse.Id);
            if (warehouseFromDB == null)
            {
                _warehouseRepository.Insert(warehouse);
            }
            else
            {
                _warehouseRepository.Update(warehouse);
            }
            await _warehouseRepository.SaveChangesAsync();

            return warehouseFromDB?.Id ?? warehouse.Id;
        }
        public async Task Remove(int id)
        {
            var warehouse = await _warehouseRepository.GetSingleAsync(w => w.Id == id);
            _warehouseRepository.Delete(warehouse);
            await _warehouseRepository.SaveChangesAsync();
        }

        public async Task Remove(List<int> ids)
        {
            if (ids == null) throw new ArgumentNullException(nameof(ids));

            var warehouses = await _warehouseRepository.FindByAsync(w => ids.Any(a => a == w.Id));

            foreach (var warehouse in warehouses)
            {
                _warehouseRepository.Delete(warehouse);
            }

            await _warehouseRepository.SaveChangesAsync();
        }
    }
}
