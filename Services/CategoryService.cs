﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CodeFirst.Models;
using Core.Data;
using Core.Services;
using Microsoft.Win32;

namespace Services
{
   public class CategoryService : ICategoryService
    {
        private readonly IRepository<Category> _categoryRepository;
        private bool _disposed;

        public CategoryService(IRepository<Category> categoryRepository)
        {

            _categoryRepository = categoryRepository;
        }

        public Task<List<Category>> GetAllCategories()
        {
            return _categoryRepository.GetAllAsync();
        }

        public Task<List<Category>> GetAllCategoriesWithSubcategories()
        {
            return _categoryRepository.GetAllIncludingAsync(cat=>cat.Subcategories);
        }
        public Task<Category> GetCategoryById(int? id)
        {
            return _categoryRepository.GetSingleIncludingAsync(w => w.Id == id);
        }

        public async Task<int> AddOrUpdate(Category category)
        {
            if (category == null) throw new ArgumentNullException(nameof(category));

            var categoryFromDB = await _categoryRepository.GetSingleIncludingAsync(w => w.Id == category.Id);
            if (categoryFromDB == null)
            {
                _categoryRepository.Insert(category);
            }
            else
            {
                _categoryRepository.Update(category);
            }
            await _categoryRepository.SaveChangesAsync();

            return categoryFromDB?.Id ?? category.Id;
        }
        public async Task Remove(int id)
        {
            var category = await _categoryRepository.GetSingleAsync(w => w.Id == id);
            _categoryRepository.Delete(category);
            await _categoryRepository.SaveChangesAsync();
        }

        public async Task Remove(List<int> ids)
        {
            if (ids == null) throw new ArgumentNullException(nameof(ids));

            var categories = await _categoryRepository.FindByAsync(w => ids.Any(a => a == w.Id));

            foreach (var category in categories)
            {
                _categoryRepository.Delete(category);
            }

            await _categoryRepository.SaveChangesAsync();
        }
    }
}
