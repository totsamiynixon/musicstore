﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CodeFirst.Abstract;
using CodeFirst.Context;
using CodeFirst.Models;

namespace CodeFirst.Concrete
{
    public class EFCourierRepository : ICourierRepository
    {
        EFContext context = new EFContext();

        public void SaveCourier(Courier courier)
        {
            if (courier.Id == 0)
                context.Couriers.Add(courier);
            else
            {
                Courier dbEntry = context.Couriers.Find(courier.Id);
                if (dbEntry != null)
                {
                    dbEntry.Name = courier.Name;
                    dbEntry.Email = courier.Email;
                    dbEntry.Orders = courier.Orders;
                    dbEntry.Phone = courier.Phone;
                    dbEntry.Stocks = courier.Stocks;
                }
            }
            context.SaveChanges();
        }
        public IEnumerable<Courier> Couriers
        {
            get { return context.Couriers; }
        }
        public Courier DeleteCourier(int Id)
        {
            Courier dbEntry = context.Couriers.Find(Id);
            if (dbEntry != null)
            {
                context.Couriers.Remove(dbEntry);
                context.SaveChanges();
            }
            return dbEntry;
        }
    }
}
