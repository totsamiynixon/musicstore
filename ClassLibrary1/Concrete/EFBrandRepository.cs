﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CodeFirst.Abstract;
using CodeFirst.Context;
using CodeFirst.Models;

namespace CodeFirst.Concrete
{
    public class EFBrandRepository : IBrandRepository
    {
        EFContext context = new EFContext();

        public void SaveBrand(Brand brand)
        {
            if (brand.Id == 0)
                context.Brands.Add(brand);
            else
            {
                Brand dbEntry = context.Brands.Find(brand.Id);
                if (dbEntry != null)
                {
                    dbEntry.Name = brand.Name;
                    dbEntry.Description = brand.Description;
                    dbEntry.Products = brand.Products;
                }
            }
            context.SaveChanges();
        }
        public IEnumerable<Brand> Brands
        {
            get { return context.Brands; }
        }
        public Brand DeleteBrand(int Id)
        {
            Brand dbEntry = context.Brands.Find(Id);
            if (dbEntry != null)
            {
                context.Brands.Remove(dbEntry);
                context.SaveChanges();
            }
            return dbEntry;
        }
    }
}
