﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CodeFirst.Abstract;
using CodeFirst.Context;
using CodeFirst.Models;

namespace CodeFirst.Concrete
{
    public class EFOrderRepository : IOrderRepository
    {
        EFContext context = new EFContext();

        public void SaveOrder(Order order)
        {
            if (order.Id == 0)
                context.Orders.Add(order);
            else
            {
                Order dbEntry = context.Orders.Find(order.Id);
                if (dbEntry != null)
                {
                    dbEntry.Products = order.Products;
                    dbEntry.CostOfOrder = order.CostOfOrder;
                    dbEntry.DeliveryAddress = order.DeliveryAddress;
                    dbEntry.DeliveryTime = order.DeliveryTime;
                    dbEntry.NumberOfProducts = order.NumberOfProducts;
                    dbEntry.UserId = order.UserId;
                }
            }
            context.SaveChanges();
        }
        public IEnumerable<Order> Orders
        {
            get { return context.Orders; }
        }
        public Order DeleteOrder(int Id)
        {
            Order dbEntry = context.Orders.Find(Id);
            if (dbEntry != null)
            {
                context.Orders.Remove(dbEntry);
                context.SaveChanges();
            }
            return dbEntry;
        }
    }
}
