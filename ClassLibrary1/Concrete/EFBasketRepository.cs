﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CodeFirst.Abstract;
using CodeFirst.Context;
using CodeFirst.Models;

namespace CodeFirst.Concrete
{
    public class EFBasketRepository : IBasketRepository
    {
        EFContext context = new EFContext();

        public void SaveBasket(Basket basket)
        {
            if (basket.Id == 0)
                context.Baskets.Add(basket);
            else
            {
                Basket dbEntry = context.Baskets.Find(basket.Id);
                if (dbEntry != null)
                {
                    dbEntry.Products = basket.Products;
                }
            }
            context.SaveChanges();
        }
        public IEnumerable<Basket> Baskets
        {
            get { return context.Baskets; }
        }
        public Basket DeleteBasket(int Id)
        {
            Basket dbEntry = context.Baskets.Find(Id);
            if (dbEntry != null)
            {
                context.Baskets.Remove(dbEntry);
                context.SaveChanges();
            }
            return dbEntry;
        }
    }
}
