﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CodeFirst.Abstract;
using CodeFirst.Context;
using CodeFirst.Models;

namespace CodeFirst.Concrete
{
    public class EFProductRepository : IProductRepository
    {
        EFContext context = new EFContext();

        public void SaveProduct(Product product)
        {
            if (product.Id == 0)
                context.Products.Add(product);
            else
            {
                Product dbEntry = context.Products.Find(product.Id);
                if (dbEntry != null)
                {
                    dbEntry.Name = product.Name;
                    dbEntry.Description = product.Description;
                    dbEntry.Price = product.Price;
                    dbEntry.Subcategory = product.Subcategory;
                    dbEntry.Images = product.Images;
                    dbEntry.Brand = product.Brand;
                }
            }
            context.SaveChanges();
        }
        public IEnumerable<Product> Products
        {
            get { return context.Products; }
        }
        public Product DeleteProduct(int Id)
        {
            Product dbEntry = context.Products.Find(Id);
            if (dbEntry != null)
            {
                context.Products.Remove(dbEntry);
                context.SaveChanges();
            }
            return dbEntry;
        }
    }
}
