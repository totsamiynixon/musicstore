﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CodeFirst.Abstract;
using CodeFirst.Context;
using CodeFirst.Models;

namespace CodeFirst.Concrete
{
    public class EFSubcategoryRepository : ISubcategoryRepository
    {
        EFContext context = new EFContext();

        public void SaveSubcategory(Subcategory subcategory)
        {
            if (subcategory.Id == 0)
                context.Subcategories.Add(subcategory);
            else
            {
                Subcategory dbEntry = context.Subcategories.Find(subcategory.Id);
                if (dbEntry != null)
                {
                    dbEntry.Name = subcategory.Name;
                    dbEntry.Description = subcategory.Description;
                    dbEntry.Category = subcategory.Category;
                    dbEntry.Products = subcategory.Products;
                }
            }
            context.SaveChanges();
        }
        public IEnumerable<Subcategory> Subcategories
        {
            get { return context.Subcategories; }
        }
        public Subcategory DeleteSubcategory(int Id)
        {
            Subcategory dbEntry = context.Subcategories.Find(Id);
            if (dbEntry != null)
            {
                context.Subcategories.Remove(dbEntry);
                context.SaveChanges();
            }
            return dbEntry;
        }
    }
}
