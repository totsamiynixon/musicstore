﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CodeFirst.Abstract;
using CodeFirst.Context;
using CodeFirst.Models;

namespace CodeFirst.Concrete
{
    public class EFStockRepository : IStockRepository
    {
        EFContext context = new EFContext();

        public void SaveStock(Stock stock)
        {
            if (stock.Id == 0)
                context.Stocks.Add(stock);
            else
            {
                Stock dbEntry = context.Stocks.Find(stock.Id);
                if (dbEntry != null)
                {
                    
                    dbEntry.Products = stock.Products;
                }
            }
            context.SaveChanges();
        }
        public IEnumerable<Stock> Stocks
        {
            get { return context.Stocks; }
        }
        public Stock DeleteStock(int Id)
        {
            Stock dbEntry = context.Stocks.Find(Id);
            if (dbEntry != null)
            {
                context.Stocks.Remove(dbEntry);
                context.SaveChanges();
            }
            return dbEntry;
        }
    }
}
