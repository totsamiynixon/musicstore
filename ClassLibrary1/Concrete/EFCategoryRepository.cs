﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CodeFirst.Abstract;
using CodeFirst.Context;
using CodeFirst.Models;

namespace CodeFirst.Concrete
{
    public class EFCategoryRepository : ICategoryRepository
    {
        EFContext context = new EFContext();

        public void SaveCategory(Category category)
        {
            if (category.Id == 0)
                context.Categories.Add(category);
            else
            {
                Category dbEntry = context.Categories.Find(category.Id);
                if (dbEntry != null)
                {
                    dbEntry.Name = category.Name;
                    dbEntry.Description = category.Description;
                    dbEntry.Subcategories = category.Subcategories;
                }
            }
            context.SaveChanges();
        }
        public IEnumerable<Category> Categories
        {
            get { return context.Categories; }
        }
        public Category DeleteCategory(int Id)
        {
            Category dbEntry = context.Categories.Find(Id);
            if (dbEntry != null)
            {
                context.Categories.Remove(dbEntry);
                context.SaveChanges();
            }
            return dbEntry;
        }
    }
}
