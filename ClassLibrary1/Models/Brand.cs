﻿using System.Collections.Generic;
using Newtonsoft.Json;

namespace CodeFirst.Models
{
    public class Brand : BaseEntity
    {
        [JsonProperty(PropertyName = "name")]
        public string Name { get; set; }
        [JsonProperty(PropertyName = "description")]
        public string Description { get; set; }
        [JsonIgnore]
        public virtual List<Product> Products { get; set; }
    }
}