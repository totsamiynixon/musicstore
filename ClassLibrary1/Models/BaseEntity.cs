﻿using Newtonsoft.Json;

namespace CodeFirst.Models
{
    public class BaseEntity
    {
        [JsonProperty(PropertyName = "id")]
        public int Id { get; set; }
    }
}