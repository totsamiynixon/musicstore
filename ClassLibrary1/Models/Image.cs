﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CodeFirst.Models
{
    public class Image : BaseEntity
    {

        [Required]
        [StringLength(512)]
        public string RelatedUrl { get; set; }

        [Required]
        public int ProductId { get; set; }

    }
}
