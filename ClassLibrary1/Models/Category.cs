﻿using System.Collections.Generic;
using Newtonsoft.Json;

namespace CodeFirst.Models
{
    public class Category : BaseEntity
    {
        [JsonProperty(PropertyName = "name")]
        public string Name { get; set; }

        [JsonIgnore]
        public virtual List<Subcategory> Subcategories { get; set; }
    }
}