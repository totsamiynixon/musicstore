﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using CodeFirst.Enums;

namespace CodeFirst.Models
{
    public class Order : BaseEntity
    {
        public string DeliveryAddress { get; set; }
        public DeliveryType DeliveryType { get; set; }
        public double OrderSum { get; set; }
        //public int CountOfProduct { get; set; }
        public virtual List<Stock> Stock { get; set; }


    }
}
