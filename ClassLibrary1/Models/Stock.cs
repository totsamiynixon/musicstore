﻿using System;
using System.Collections.Generic;

namespace CodeFirst.Models
{
    public class Stock : BaseEntity
    {
        public int ProductId { get; set; }
        public virtual Product Product { get; set; }
        public int NumberOfProducts { get; set; }

        public int WarehouseId { get; set; }
        public virtual Warehouse Warehouse { get; set; }

    }
}