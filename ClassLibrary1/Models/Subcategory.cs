﻿using System.Collections.Generic;
using Newtonsoft.Json;

namespace CodeFirst.Models
{
    public class Subcategory : BaseEntity
    {
        [JsonProperty(PropertyName = "name")]
        public string Name { get; set; }
        [JsonIgnore]
        public virtual List<Product> Products { get; set; }
        [JsonProperty(PropertyName = "categoryId")]
        public int CategoryId { get; set; }
        [JsonIgnore]
        public virtual Category Category { get; set; }
    }
}