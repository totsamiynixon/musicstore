﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CodeFirst.Models
{
    public class Basket : BaseEntity
    {
        public int ApplicationUserId { get; set; }
        public virtual List<Product> Products { get; set; }
    }
}
