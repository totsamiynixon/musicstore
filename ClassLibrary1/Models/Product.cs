﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CodeFirst.Models
{
    public class Product : BaseEntity
    {
        public string Name { get; set; }
        public double Price { get; set; }
        public string Description { get; set; }
        public int BrandId { get; set; }
        public int SubcategoryId { get; set; }
        public int DefaultImageId { get; set; }
        public virtual Brand Brand { get; set; }
        public virtual Subcategory Subcategory { get; set; }
        public virtual List<Stock> Stocks { get; set; }
        public virtual List<Image> Images { get; set; }
         //public virtual Stock Stock { get; set; }

    }
}
