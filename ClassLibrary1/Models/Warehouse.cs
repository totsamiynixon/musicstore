﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CodeFirst.Models
{
    public class Warehouse : BaseEntity
    {
        public string Address { get; set; }

        public virtual List<Stock> Stocks { get; set; }
    }
}
