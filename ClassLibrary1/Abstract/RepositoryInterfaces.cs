﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CodeFirst.Models;

namespace CodeFirst.Abstract
{
    public interface IProductRepository
    {
        IEnumerable<Product> Products { get; }
        void SaveProduct(Product product);
        Product DeleteProduct(int Id);
    }

    public interface IBrandRepository
    {
        IEnumerable<Brand> Brands { get; }
        void SaveBrand(Brand brand);
        Brand DeleteBrand(int Id);
    }

    public interface IBasketRepository
    {
        IEnumerable<Basket> Baskets { get; }
        void SaveBasket(Basket basket);
       Basket DeleteBasket(int Id);
    }

    public interface ICategoryRepository
    {
        IEnumerable<Category> Categories { get; }
        void SaveCategory(Category category);
        Category DeleteCategory(int Id);
    }


    public interface ICourierRepository
    {
        IEnumerable<Courier>  Couriers{ get; }
        void SaveCourier(Courier courier);
        Courier DeleteCourier(int Id);
    }

    public interface IOrderRepository
    {
        IEnumerable<Order> Orders { get; }
        void SaveOrder(Order order);
        Order DeleteOrder(int Id);
    }

    public interface IStockRepository
    {
        IEnumerable<Stock> Stocks { get; }
        void SaveStock(Stock stock);
        Stock DeleteStock(int Id);
    }

    public interface ISubcategoryRepository
    {
        IEnumerable<Subcategory> Subcategories { get; }
        void SaveSubcategory(Subcategory subcategory);
        Subcategory DeleteSubcategory(int Id);
    }
}

