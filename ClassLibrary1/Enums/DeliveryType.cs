﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CodeFirst.Enums
{
    public enum DeliveryType
    {
        SeflGetting = 1,
        FreeShipping = 2,
        PaidDelivery = 3
    }
}
