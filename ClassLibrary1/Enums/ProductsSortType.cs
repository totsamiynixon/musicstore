﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CodeFirst.Enums
{
    public enum ProductsSortType
    {
        PriceAscending = 1,
        PriceDescending = 2,
        NameAscending = 3,
        NameDescending = 4
    }
}
