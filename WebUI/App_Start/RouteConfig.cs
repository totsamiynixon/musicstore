﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;
using System.Web.Mvc;
using System.Web.Routing;

namespace WebUI
{  //SPECIAL FOR ANGULAR 2

//    #region Angular2

//    public class ServerRouteConstraint : IRouteConstraint
//    {
//        private readonly Func<Uri, bool> _predicate;

//        public ServerRouteConstraint(Func<Uri, bool> predicate)
//        {
//            this._predicate = predicate;
//        }

//        public bool Match(HttpContextBase httpContext, Route route, string parameterName,
//            RouteValueDictionary values, RouteDirection routeDirection)
//        {
//            return this._predicate(httpContext.Request.Url);
//        }
//    }

//#endregion 
    public class RouteConfig
    {
        public static void RegisterRoutes(RouteCollection routes)
        {

            routes.IgnoreRoute("{resource}.axd/{*pathInfo}");
            // routes.IgnoreRoute("Angular2/category");



            routes.MapRoute(
            name: "InitProductsRoute",
            url: "api.customer.catalog/init",
            defaults: new { controller = "Customer", action = "Init" }
        );
            //    routes.MapRoute(
            //    name: "InitRoute",
            //    url: "Customer/Catalog/api/init",
            //    defaults: new { controller = "Customer", action = "Init" }
            //);
            routes.MapRoute(
            name: "CatalogDefault",
            url: "Customer/Catalog",
            defaults: new { controller = "Customer", action = "Index" }
        );
            routes.MapRoute(
             name: "CatalogRoute",
             url: "Customer/Catalog/{subcategoryName}",
             defaults: new { controller = "Customer", action = "Catalog", subcategoryName = UrlParameter.Optional }
         );


            routes.MapRoute(
                      name: "spa-fallback",
                      url: "Angular2/{*catchall}",
                      defaults: new { controller = "Angular2", action = "Index" }
       );

            routes.MapRoute(
                name: "Default",
                url: "{controller}/{action}/{id}",
                defaults: new { controller = "Home", action = "Index", id = UrlParameter.Optional }

            );

           

        }
    }
}
