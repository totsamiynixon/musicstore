﻿
using System;
using System.Net.Http.Formatting;
using System.Web.Http;
using System.Xml;
using Ninject.Activation;

namespace WebUI
{
    public static class WebApiConfig
    {
        public static void Register(HttpConfiguration config)
        {
            config.MapHttpAttributeRoutes();

            config.Routes.MapHttpRoute(
           name: "GetProductsRoute",
           routeTemplate: "api.customer.catalog/getProducts",
           defaults: new { controller = "ProductWeb" }
       );


            // //JsonP POLICY 
            // config.Routes.MapHttpRoute(
            //    name: "DefaultJsonpApi",
            //    routeTemplate: "api/{controller}/{id}/{format}",
            //    defaults: new { id = RouteParameter.Optional, format = RouteParameter.Optional }
            //);
            config.Routes.MapHttpRoute(
              name: "PaginationChanges",
              routeTemplate: "api/ProductWeb/PaginationChanges",
              defaults: new {controller = "ProductWeb", action="PaginationChanges"}
          );
            config.Routes.MapHttpRoute(
              name: "SubcategoryChanges",
              routeTemplate: "api/ProductWeb/SubcategoryChanges",
              defaults: new { controller = "ProductWeb", action = "SubcategoryChanges" }
          );
            config.Routes.MapHttpRoute(
              name: "CategoryChanges",
              routeTemplate: "api/ProductWeb/CategoryChanges",
              defaults: new { controller = "ProductWeb", action = "CategoryChanges" }
          );
            config.Routes.MapHttpRoute(
              name: "SidebarChanges",
              routeTemplate: "api/ProductWeb/SidebarChanges",
              defaults: new { controller = "ProductWeb", action = "SidebarChanges" }
          );
            //standartRoute
            config.Routes.MapHttpRoute(
             name: "DefaultApi",
             routeTemplate: "api/{controller}/{id}",
             defaults: new { id = RouteParameter.Optional }
         );
            GlobalConfiguration.Configuration.Formatters.JsonFormatter.MediaTypeMappings
.Add(new RequestHeaderMapping("Accept",
                              "text/html",
                              StringComparison.InvariantCultureIgnoreCase,
                              true,
                              "application/json"));
        }

    }
}
