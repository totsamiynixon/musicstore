﻿
function BrandItem(id, name, description) {
    var self = this;

    self.id = ko.observable(id);
    self.Name = ko.observable(name);
    self.Selected = ko.observable(false);
    self.Description = ko.observable(description);
}

var ProductViewModel = function () {
    this.products = ko.observableArray();
    this.subcategoryId = ko.observable();
    this.brands = ko.observableArray();
    this.associatedBrands = ko.observableArray();

    this.setCurrentSubcategoryId = function(data, element) {
        //function (element) {
        //alert(element.target.id);
        //
        this.subcategoryId(element.target.id);
        this.getAllItems();
    }
    this.getAllItems = function() {
        var that = this;
            sendAjaxRequest("GET",
                function(data) {
                    that.products.removeAll();
                    that.brands.removeAll();
                    for (var i = 0; i < data.Products.length; i++) {
                        that.products.push(data.Products[i]);
                    }
                var brands = [];
                for (var i = 0; i < data.Brands.length; i++) {
                    brands.push(data.Brands[i])
                }
                for (var i = 0; i < brands.length; i++) {
                    that.brands.push(new BrandItem(brands[i].Id, brands[i].Name, brands[i].Description));
                };
            },
            "?" + "subcategoryId=" + that.subcategoryId());
    }

};

$(".catalog-navigation-item").hover(
    function () {
        var right = (parseInt($(this).attr("data-id")) - 1) * 120;
        right = 0 - right;
        right = right.toString() + "px";
        $(this).find(".dropdown-content").css("left", right).show();
    },
    function () {
        $(this).find(".dropdown-content").hide();
    }
    );
$(".products").hover(
    function () {
        $(this).hide();
    });

function sendAjaxRequest(httpMethod, callback, url) {
    $.ajax("/api/productweb" + (url ? url : ""), {
        type: httpMethod,
        success: callback
    });
}



function removeItem(item) {
    sendAjaxRequest("DELETE", function () {
        getAllItems()
    }, item.ProductId);
}

$(function () {
    ko.applyBindings(new ProductViewModel());
});














//var model = {
//    products: ko.observableArray(),
//    subcategoryId: ko.observable()
//    };

//function sendAjaxRequest(httpMethod, callback, url) {
//    $.ajax("/api/productweb" + (url ? "/" + url : ""), {
//        type: httpMethod,
//        success: callback
//    });
//}

//function getAllItems() {
   

//        sendAjaxRequest("GET",
//            function(data) {
//                model.products.removeAll();
//                for (var i = 0; i < data.length; i++) {
//                    model.products.push(data[i]);
//                }
//            },
//            "?" + "subcategoryId=" + model.subcategoryId());
//}

//function removeItem(item) {
//    sendAjaxRequest("DELETE", function () {
//        getAllItems()
//    }, item.ProductId);
//}

////function getCurrentSubcategoryId() {
////    var url = window.location.pathname.toString();
////    url = url.split("/");
////    sendAjaxRequest("GET",
////        function(data) {
////            model.subcategoryId(data.Id);
////            getAllItems();
////        },
////        "GetSubcategoryId/" + url[2]);
////}

//function setCurrentSubcategoryId(element) {
//    model.subcategoryId(element.target.id);
//    getAllItems();
//}

//$(".categories_menu ul ul li a").click(
//    //function (element) {
//    //alert(element.target.id);
//    //}
//    function(element) {
//        setCurrentSubcategoryId(element);
//    }
//);

//$(function () {
//    //getCurrentSubcategoryId();
//    ko.applyBindings(model);
//});