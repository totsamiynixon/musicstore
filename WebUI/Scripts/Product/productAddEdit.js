﻿ko.bindingHandlers.fileSrc = {
    init: function (element, valueAccessor) {
        ko.utils.registerEventHandler(element, "change", function () {
            var reader = new FileReader();

            reader.onload = function (e) {
                var value = valueAccessor();
                value(e.target.result);
            }

            reader.readAsDataURL(element.files[0]);
        });
    }
};
function CreatePreviews(data, e) {
    var files = e.target.files;
    for (var i = 0; i < files.length; i++) {
        var reader = new FileReader();
        reader.onload = function (event) {
            viewModel.UploadedImagesPreview.push(event.target.result);
        }
        reader.readAsDataURL(files[i])
       // reader.readAsDataURL(files[i]);
    }
}
function ClearPreview(data, e) {
    $("#file-form").trigger("reset");
    viewModel.UploadedImagesPreview.removeAll();
}
function UploadSuccessHandler(data) {
    viewModel.UploadedImagesPreview.removeAll();
    $.each(data.UploadedImages, function (index, value) {
        var koValue = ko.mapping.fromJS(value)
        viewModel.UploadedImages.push(koValue);
    })
    var $el = $('#uploadImage');
    $el.wrap('<form>').closest('form').get(0).reset();
    $el.unwrap();

}