﻿function CreatePreviews(data, e) {
    var files = e.target.files;
    for (var i = 0; i < files.length; i++) {
        var reader = new FileReader();
        reader.onload = function (event) {
            viewModel.UploadedImagesPreview.push(event.target.result);
        }
        reader.readAsDataURL(files[i])
        // reader.readAsDataURL(files[i]);
    }
}
function ClearPreview(data, e) {
    $("#file-form").trigger("reset");
    viewModel.UploadedImagesPreview.removeAll();
}
$(document).ready(function () {
    $(".single").select2({
        maximumSelectionLength: 1
    });
});