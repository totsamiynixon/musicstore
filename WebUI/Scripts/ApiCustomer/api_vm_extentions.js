﻿function CartProductFromLocalStorage(product, count_in_cart) {
    this.product = product;
    this.count_in_cart = ko.observable(count_in_cart);
}
function CartProduct(product) {
    this.product = product;
    this.count_in_cart = ko.observable(1);
}

function Cart() {
    var self = this;
    self.products = ko.observableArray();
    this.dirty_flag = ko.DirtyFlag(self.products);
    this.pause_dirty = ko.observable(false);
    this.total_count = ko.observable(0);
    this.clear_dirty = function() {
        self.dirty_flag().reset();
    };
    this.take_from_storage = function() {
        var products = JSON.parse(localStorage["Cart"]);
        products = JSON.parse(products);
        products.forEach(function(product, i) {
            self.products.push(new CartProductFromLocalStorage(product.product,product.count_in_cart))
        });

    }
    this.take_from_storage();
    this.add_to_cart = function (product) {
        if (self.products().length > 0) {
            var productLength = self.products().length;
            var foundedIndex = "";
            for (var i = 0; i < productLength; i++) {
                if (self.products()[i].product.id === product.id) {
                    foundedIndex = i;
                }
            }
            if (foundedIndex !== "") {
                var countInCart = self.products()[foundedIndex].count_in_cart() + 1;
                self.products()[foundedIndex].count_in_cart(countInCart);
            } else {
            self.products.push(new CartProduct(product));
            }
        } else {
            var cartProduct = new CartProduct(product);
            self.products.push(cartProduct);

        }
        this.add_to_a_localstorage(self);
    }

    this.add_to_a_localstorage = function (cart) {
        var localstorageProducts = JSON.stringify(ko.mapping.toJSON(cart.products));
        localStorage.clear();
        localStorage["Cart"] = localstorageProducts;
        console.log(localStorage["Cart"]);

    }

    this.total_price = ko.computed(function () {
        if (self.dirty_flag().isDirty() && !self.pause_dirty()) {
            var totalPrice = 0;
            var totalCount = 0;
            self.clear_dirty();
            if (self.products().length !== 0) {
                for (var i = 0; i < self.products().length; i++) {
                    totalPrice = totalPrice + self.products()[i].product.price * self.products()[i].count_in_cart();
                    totalCount = totalCount + self.products()[i].count_in_cart();
                }
            }
            self.total_count(totalCount);
            return totalPrice;
        }
    });
}
function Pagination(data) {
    this.current_page = ko.observable(data.current_page);
    this.items_per_page = ko.observable(data.items_per_page);
    this.pages = ko.observableArray([]); 
    for (var i = 0; i < data.pages.length; i++) {
        this.pages.push(data.pages[i]);
    }
    this.current_sort_type = ko.observable(data.current_sort_type);
}
function Subcategory(data) {
    this.id = data.id;
    this.name = data.name;
    this.category_id = data.category_id;
}
function Category(data) {
    var self = this;
    this.id = data.id;
    this.name = data.name;
    this.subcategories = ko.observableArray([]);
    for (var i = 0; i < data.subcategories.length; i++) {
        self.subcategories.push(new Subcategory(data.subcategories[i]));
    }
}
function Brand(data) {
    var self = this;
    this.id = data.id;
    this.name = data.name;
    this.description = data.description;
    this.is_chosen = ko.observable(data.is_chosen);
}
function Sidebar(sidebar) {
    var self = this;
    this.brands = ko.observableArray([]);
    for (var i = 0; i < sidebar.brands.length; i++) {
        self.brands.push(new Brand(sidebar.brands[i]));
    }
    this.start_price = ko.observable(sidebar.start_price);
    //ko.computed(function() {
    //        if (self.start_price() === "") {
    //            self.start_price(0);
    //        }
    //    },
    //    self.start_price);
    this.finish_price = ko.observable(sidebar.finish_price);
    //ko.computed(function () {
    //    if (self.finish_price() === "") {
    //        self.finish_price(10000);
    //    }
    //},
    //   self.finish_price);
    this.categories = ko.observableArray([]);
    for (var i = 0; i < sidebar.categories.length; i++) {
        self.categories.push(new Category(sidebar.categories[i]));
    }


}

function ViewModel(data)
{
    var self = this;
    this.current_category_Id = ko.observable(data.current_category_id);
    this.sidebar = ko.observable(new Sidebar(data.sidebar));
    this.pagination = ko.observable(new Pagination(data.pagination));
    this.products = ko.observableArray();
    for (var i = 0; i < data.products.length; i++) {
        self.products.push(data.products[i]);
    }
    this.current_subcategory_id = ko.observable(data.current_subcategory_id);
    this.cart = ko.observable(new Cart());

   this.setCurrentCategory = function(data, e) {
       this.current_category_Id(data.id);
       this.current_subcategory_id(0);
   }

   this.setCurrentSubcategory = function (data, e) {
       this.current_subcategory_id(data.id);
       this.sendAjaxRequestAfterSubcategoryChanged();
   }
   this.setCurrentPage = function (value) {
           self.pagination().current_page(value);
   }
    //Pagination dirty catcher
   this.pagination_dirty_flag = ko.DirtyFlag(self.pagination);
   this.pagination_pause_dirty = ko.observable(false);
  
   this.clear_pagination_dirty =  function(){
       self.pagination_dirty_flag().reset();
    }

    this.pagination_status = ko.computed(function() {
        if (self.pagination_dirty_flag().isDirty()&& !self.pagination_pause_dirty()) {
            self.clear_pagination_dirty();
            self.sendAjaxRequestAfterPaginationChanged();
        }
    });
    //Sidebar catcher
    this.sidebar_dirty_flag = ko.DirtyFlag(self.sidebar);
    this.sidebar_pause_dirty = ko.observable(false);
    this.clear_sidebar_dirty = function () {
        self.sidebar_dirty_flag().reset();
    }

    this.sidebar_status = ko.computed(function () {
        if (self.sidebar_dirty_flag().isDirty() && !self.sidebar_pause_dirty()) {
            self.clear_sidebar_dirty();
            self.sendAjaxRequestAfterSidebarChanged();
        }
    });

    //Current subcategory dirty catcher
    this.subcategory_dirty_flag = ko.DirtyFlag(self.current_subcategory_id());


    this.clear_subcategory_dirty = function () {
        self.subcategory_dirty_flag().reset();
    }

    this.subcategory_status = ko.computed(function () {
        if (self.subcategory_dirty_flag().isDirty()) {
            self.clear_subcategory_dirty();
            self.sendAjaxRequestAfterSubcategoryChanged();
        }
    });

    //Current category dirty catcher
    this.category_dirty_flag = ko.DirtyFlag(self.current_category_Id);

    this.clear_category_dirty = function () {
        self.category_dirty_flag().reset();
    }

    this.category_status = ko.computed(function () {
        if (self.category_dirty_flag().isDirty()) {
            self.clear_category_dirty();
            self.sendAjaxRequestAfterCategoryChanged();
        }
    });
    this.sendAjaxRequestAfterVMChanged = function () {

        var model = ko.toJS(self);


        $.ajax({
            url:
                //"/Customer/GetAllProducts/",
                "/api.customer.catalog/PaginationChanges",
            dataType: "json",
            data: ko.toJSON(self.prepareModel(self)),
            type: "POST",
            contentType: "application/json",
            success: function (data) {
                var products = JSON.parse(data);
                self.products.removeAll();
                for (var i = 0; i < products.length; i++) {
                    self.products.push(products[i]);
                }
            }
        });
    }
    this.sendAjaxRequestAfterPaginationChanged = function () {

        var model = ko.toJS(self);


       $.ajax({
           url:
               //"/Customer/GetAllProducts/",
               "/api/ProductWeb/PaginationChanges",
           dataType: "json",
           data: ko.toJSON(self.preparePaginationModel(self)),
           type: "POST",
           contentType: "application/json",
           success: function (data) {
               var products = JSON.parse(data);
               self.products.removeAll();
               for (var i = 0; i < products.length; i++) {
                   self.products.push(products[i]);
               }
           }
       });
    }
    this.preparePaginationModel = function(self) {
        var model = ko.toJS(self);
        model.products= [];
        model.sidebar.categories = [];
        model.sidebar.start_price === "" ? model.sidebar.start_price = 0 : model.sidebar.start_price = model.sidebar.start_price;
        model.sidebar.finish_price === "" ? model.sidebar.finish_price = 10000 : model.sidebar.finish_price = model.sidebar.finish_price;
        return model;
    }



    this.sendAjaxRequestAfterCategoryChanged = function () {

        var model = ko.toJS(self);


        $.ajax({
            url:
                //"/Customer/GetAllProducts/",
                "/api/ProductWeb/CategoryChanges",
            dataType: "json",
            data: ko.toJSON(self.prepareCategoryModel(self)),
            type: "POST",
            contentType: "application/json",
            success: function (data) {
                data = JSON.parse(data);
                self.products.removeAll();
                for (var i = 0; i < data.products.length; i++) {
                    self.products.push(data.products[i]);
                }
                self.sidebar_pause_dirty(true);
                self.sidebar().brands.removeAll();
                for (var i = 0; i < data.sidebar.brands.length; i++) {
                    self.sidebar().brands.push(new Brand(data.sidebar.brands[i]));
                }
                self.pagination_pause_dirty(true);
                self.pagination(new Pagination(data.pagination));
                self.clear_pagination_dirty();
                self.clear_sidebar_dirty();
                self.sidebar_pause_dirty(false);
                self.pagination_pause_dirty(false);

            }
        });
    }
    this.prepareCategoryModel = function (self) {
        var model = ko.toJS(self);
        model.products = [];
        model.sidebar.categories = [];
        model.sidebar.brands = [];
        model.sidebar.start_price === "" ? model.sidebar.start_price = 0 : model.sidebar.start_price = model.sidebar.start_price;
        model.sidebar.finish_price === "" ? model.sidebar.finish_price = 10000 : model.sidebar.finish_price = model.sidebar.finish_price;
        return model;
    }

    this.sendAjaxRequestAfterSubcategoryChanged = function () {

        var model = ko.toJS(self);


        $.ajax({
            url:
                //"/Customer/GetAllProducts/",
                "/api/ProductWeb/SubcategoryChanges",
            dataType: "json",
            data: ko.toJSON(self.prepareCategoryModel(self)),
            type: "POST",
            contentType: "application/json",
            success: function (data) {
                data = JSON.parse(data);
                self.products.removeAll();
                for (var i = 0; i < data.products.length; i++) {
                    self.products.push(data.products[i]);
                }
                self.sidebar_pause_dirty(true);
                self.sidebar().brands.removeAll();
                for (var i = 0; i < data.sidebar.brands.length; i++) {
                    self.sidebar().brands.push(new Brand(data.sidebar.brands[i]));
                }
                self.pagination_pause_dirty(true);
                self.pagination(new Pagination(data.pagination));
                self.clear_pagination_dirty();            
                self.clear_sidebar_dirty();
                self.pagination_pause_dirty(false);
                self.sidebar_pause_dirty(false);

            }
        });
    }
    this.prepareSubcategoryyModel = function (self) {
        var model = ko.toJS(self);
        model.products = [];
        model.sidebar.categories = [];
        model.sidebar.brands = [];
        model.sidebar.start_price === "" ? model.sidebar.start_price = 0 : model.sidebar.start_price = model.sidebar.start_price;
        model.sidebar.finish_price === "" ? model.sidebar.finish_price = 10000 : model.sidebar.finish_price = model.sidebar.finish_price;
        return model;
    }

    this.sendAjaxRequestAfterSidebarChanged = function () {

        var model = ko.toJS(self);


        $.ajax({
            url:
                //"/Customer/GetAllProducts/",
                "/api/ProductWeb/SidebarChanges",
            dataType: "json",
            data: ko.toJSON(self.prepareSidebarModel(self)),
            type: "POST",
            contentType: "application/json",
            success: function (data) {
                data = JSON.parse(data);
                self.products.removeAll();
                for (var i = 0; i < data.products.length; i++) {
                    self.products.push(data.products[i]);
                }
                self.pagination_pause_dirty(true);
                self.pagination(new Pagination(data.pagination));
                self.clear_pagination_dirty();
                self.pagination_pause_dirty(false);

            }
        });
    }
    this.prepareSidebarModel = function (self) {
        var model = ko.toJS(self);
        model.products = [];
        model.sidebar.categories = [];
        model.sidebar.start_price === "" ? model.sidebar.start_price = 0 : model.sidebar.start_price = model.sidebar.start_price;
        model.sidebar.finish_price === "" ? model.sidebar.finish_price = 10000 : model.sidebar.finish_price = model.sidebar.finish_price;
        return model;
    }




    this.set_modal_visible = function(elementClass) {
        creatOverlay();
        showModal(elementClass);
    }
    this.set_modal_invisible = function(element) {
        $(element).remove();
        $(".modal").hide().removeClass("modal");
    }

}

function init() {
    $.ajax({
            dataType: "json",
            url: "/api.customer.catalog/init",
               // "Catalog/api/init",
            success: function (data) {
               // var json_data = ko.mapping.toJSON(data);
                ko.applyBindings(new ViewModel(JSON.parse(data)));
            },
            type: "POST"
    });
}



function creatOverlay() {
    var docHeight = $(document).height();

    $("<div id='overlay' onclick='hideModal(this)' data-bind='click: function(data,event){set_modal_invisible(event.target)}'></div>")
    .appendTo("body")
    .height(docHeight)
    .css({
        'opacity': 0.4,
        'position': 'absolute',
        'top': 0,
        'left': 0,
        'background-color': 'black',
        'width': '100%',
        'z-index': 10,
        'cursor': 'pointer'
    });
}
function showModal(elementClass) {
    $(elementClass).addClass("modal").slideDown();
}

function hideModal(element) {
    $(".modal").slideUp("slow", function(){
        $(element).remove();
        $(this).removeClass("modal")
    });
}


$.ready(init());

