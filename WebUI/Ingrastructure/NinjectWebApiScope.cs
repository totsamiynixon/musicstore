﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http.Dependencies;
using Ninject.Activation;
using Ninject.Parameters;
using Ninject.Syntax;

namespace WebUI.Ingrastructure
{
    public class NinjectWebApiScope : IDependencyScope
    {
        protected IResolutionRoot _resolutionRoot;

        public NinjectWebApiScope(IResolutionRoot resolutionRoot)
        {
            _resolutionRoot = resolutionRoot;
        }

        public object GetService(Type serviceType)
        {
            return _resolutionRoot.Resolve(this.CreateRequest(serviceType)).SingleOrDefault();

        }

        public IEnumerable<object> GetServices(Type servicesType)
        {
            return _resolutionRoot.Resolve(this.CreateRequest(servicesType));
        }

        private IRequest CreateRequest(Type serviceType)
        {
            return _resolutionRoot.CreateRequest(serviceType, null, new Parameter[0], true,true)
            ;
        }

        public void Dispose()
        {
            _resolutionRoot = null;
        }
    }
}