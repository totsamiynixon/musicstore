﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using CodeFirst.Models;
using Core.Context;
using Core.Data;
using Core.Services;
using Data.Context;
using Data.Repositories;
using Ninject;
using Services;

namespace WebUI.Ingrastructure
{
    public class NinjectDependencyResolver : IDependencyResolver
    {
        private IKernel kernel;

        public NinjectDependencyResolver(IKernel kernelParam)
        {
            kernel = kernelParam;
            AddBindings();
        }

        public object GetService(Type serviceType)
        {
            return kernel.TryGet(serviceType);
        }

        public IEnumerable<object> GetServices(Type serviceType)
        {
            return kernel.GetAll(serviceType);
        }

        private void AddBindings()
        {
            kernel.Bind<IRepository<Product>>().To<EntityRepository<Product>>();
            kernel.Bind<IRepository<Basket>>().To<EntityRepository<Basket>>();
            kernel.Bind<IRepository<Brand>>().To<EntityRepository<Brand>>();
            kernel.Bind<IRepository<Category>>().To<EntityRepository<Category>>();
            kernel.Bind<IRepository<Courier>>().To<EntityRepository<Courier>>();
            kernel.Bind<IRepository<Image>>().To<EntityRepository<Image>>();
            kernel.Bind<IRepository<Order>>().To<EntityRepository<Order>>();
            kernel.Bind<IRepository<Stock>>().To<EntityRepository<Stock>>();
            kernel.Bind<IRepository<Subcategory>>().To<EntityRepository<Subcategory>>();
            kernel.Bind<IRepository<Warehouse>>().To<EntityRepository<Warehouse>>();
            kernel.Bind<IImageService>().To<ImageService>();
            kernel.Bind<IApplicationContext>().To<ApplicationContext>();
            kernel.Bind<IBasketService>().To<BasketService>();
            kernel.Bind<IBrandService>().To<BrandService>();
            kernel.Bind<ICategoryService>().To<CategoryService>();
            kernel.Bind<ICourierService>().To<CourierService>();
            kernel.Bind<IOrderService>().To<OrderService>();
            kernel.Bind<IProductService>().To<ProductService>();
            kernel.Bind<IStockService>().To<StockService>();
            kernel.Bind<ISubcategoryService>().To<SubcategoryService>();
            kernel.Bind<IWarehouseService>().To<WarehouseService>();



        }
    }
}