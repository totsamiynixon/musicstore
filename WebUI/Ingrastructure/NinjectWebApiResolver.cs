﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http.Dependencies;
using Ninject;

namespace WebUI.Ingrastructure
{
    public class NinjectWebApiResolver : NinjectWebApiScope, IDependencyResolver
    {
        private IKernel kernel;

        public NinjectWebApiResolver(IKernel kernel) : base(kernel)
        {
            this.kernel = kernel;
        }

        public IDependencyScope BeginScope()
        {
            return new NinjectWebApiScope(kernel.BeginBlock());
        }
    }
}