﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AutoMapper;
using CodeFirst.Models;
using WebUI.Models;
using WebUI.Models.API.Brands;
using WebUI.Models.API.Categories;
using WebUI.Models.API.Images;
using WebUI.Models.API.Products;
using WebUI.Models.API.Sucategory;
using WebUI.Models.Brand;
using WebUI.Models.Products;
using WebUI.Models.Subcategories;

namespace WebUI.Ingrastructure
{
    public static class MapperService
    {
        public static void BootStrap()
        {
            Mapper.Initialize(cfg =>
            {
                cfg.CreateMap<Subcategory,SubcategoryViewModel>();
                cfg.CreateMap<Brand, BrandViewModel>();
                cfg.CreateMap<Image, ImageViewModel>().ForMember(m=>m.isDeleted, opts=>opts.Ignore());
                cfg.CreateMap<ImageViewModel, Image>();
                cfg.CreateMap<Brand, BrandViewModel>();
                // cfg.CreateMap<ProductApiViewModel, Product>();

                //Api section//

                cfg.CreateMap<Product, ApiProduct>();
                cfg.CreateMap<Image, ApiImage>();
                cfg.CreateMap<Brand, ApiBrand>().ForMember(m => m.isChosen, opts => opts.Ignore());
                cfg.CreateMap<Subcategory, ApiSubcategory>();
                cfg.CreateMap<Category, ApiCategory>();

            });
            Mapper.AssertConfigurationIsValid();
        }

        public static TResult Convert<TSource, TResult>(TSource source)
            where TResult : new()
        {
            var target = new TResult();
            Mapper.Map(source, target);

            return target;
        }

        public static List<TResult> ConvertCollection<TSource, TResult>(IList<TSource> sources)
            where TResult : new()
        {
            return sources.Select(Convert<TSource, TResult>).ToList();
        }

        public static void MapToEntity<TSource, TResult>(TSource from, TResult to)
        {
            Mapper.Map(from, to);
        }
    }
}
