﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Net;
using System.Web;
using System.Web.Mvc;
using CodeFirst.Models;
using Core.Services;
using Microsoft.AspNet.Identity.Owin;
using WebUI.Models;
using WebUI.Models.Brand;

namespace WebUI.Controllers
{
    public class BrandsController : Controller
    {
        //private ApplicationContext db
        //{
        //    get { return HttpContext.GetOwinContext().Get<ApplicationContext>(); }
        //}
        private IBrandService _brandService;

        public BrandsController(IBrandService brandService)
        {
            _brandService = brandService;
        }

        // GET: Brands
        public async Task<ActionResult> Index()
        {
            IndexModel model = new IndexModel
            {
                Brands = await _brandService.GetAllBrands()
            };
            return View(model);
        }

        // GET: Brands/Details/5
        public async Task<ActionResult> Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Brand brand = await _brandService.GetBrandById(id);
            AddEditModel brandViewModel = new AddEditModel
            {
                Id =  brand.Id,
                Name = brand.Name,
                Description = brand.Description
            };
            if (brand == null)
            {
                return HttpNotFound();
            }
            return View(brandViewModel);
        }

        // GET: Brands/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Brands/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Create( AddEditModel brandViewModel)
        {
            if (ModelState.IsValid)
            {
               Brand brand = new Brand
               {
                   Description = brandViewModel.Description,
                   Name = brandViewModel.Name
               };
               await _brandService.AddOrUpdate(brand);
                return RedirectToAction("Index");
            }

            return View(brandViewModel);
        }

        // GET: Brands/Edit/5
        public async Task<ActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Brand brand = await _brandService.GetBrandById(id);
            if (brand != null)
            {
                AddEditModel brandViewModel = new AddEditModel
                {
                    Id = brand.Id,
                    Name = brand.Name,
                    Description = brand.Description
                };
                return View(brandViewModel);
            }
            else 
            {
                return HttpNotFound();
            }
        }

        // POST: Brands/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Edit(AddEditModel brandViewModel)
        {
            if (ModelState.IsValid)
            {
                Brand brand = await _brandService.GetBrandById(brandViewModel.Id);
                if (brand != null)
                {
                    brand.Name = brandViewModel.Name;
                    brand.Description = brandViewModel.Description;
                    await _brandService.AddOrUpdate(brand);
                    return RedirectToAction("Index");
                }
                else return HttpNotFound();
            }
            return View(brandViewModel);
        }

        // GET: Brands/Delete/5
        public async Task<ActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Brand brand = await _brandService.GetBrandById(id);
            if (brand != null)
            {
                AddEditModel brandViewModel = new AddEditModel
                {
                    Id = brand.Id,
                    Name = brand.Name,
                    Description = brand.Description
                };
                return View(brandViewModel);
            }
            else 
            {
                return HttpNotFound();
            }
        }

        // POST: Brands/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> DeleteConfirmed(int id)
        {
            Brand brand = await _brandService.GetBrandById(id);
            if (brand != null)
            {
                await _brandService.Remove(id);
                return RedirectToAction("Index");
            }
            else return HttpNotFound();
        }

        //protected override void Dispose(bool disposing)
        //{
        //    if (disposing)
        //    {
        //        db.Dispose();
        //    }
        //    base.Dispose(disposing);
        //}
    }
}
