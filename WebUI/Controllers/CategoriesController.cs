﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Net;
using System.Web;
using System.Web.Mvc;

using CodeFirst.Models;
using Core.Services;
using Microsoft.AspNet.Identity.Owin;
using WebUI.Models.Categories;

namespace WebUI.Controllers
{
    public class CategoriesController : Controller
    {
        //private ApplicationContext db
        //{
        //    get { return HttpContext.GetOwinContext().Get<ApplicationContext>(); }
        //}
        private ICategoryService _categoryService;

        public CategoriesController(ICategoryService categoryService)
        {
            _categoryService = categoryService;
        }
        // GET: Categories
        public async Task<ActionResult> Index()
        {
            IndexModel model = new IndexModel
            {
                Categories = await _categoryService.GetAllCategories()
            };
            return View(model);
        }

        // GET: Categories/Details/5
        public async Task<ActionResult> Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Category category = await _categoryService.GetCategoryById(id);
            AddEditModel model = new AddEditModel
            {
                Id = category.Id,
                Name = category.Name
            };
            if (category == null)
            {
                return HttpNotFound();
            }
            return View(model);
        }

        // GET: Categories/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Categories/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Create(AddEditModel model)
        {
            if (ModelState.IsValid)
            {
                Category category = new Category
                {
                    Name = model.Name
                };
                await _categoryService.AddOrUpdate(category);
                return RedirectToAction("Index");
            }
         return View(model);
        }

        // GET: Categories/Edit/5
        public async Task<ActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Category category = await _categoryService.GetCategoryById(id);
            if (category != null)
            {
                AddEditModel model = new AddEditModel
                {
                    Id = category.Id,
                    Name = category.Name,
                };
                return View(model);
            }
            else
            {
                return HttpNotFound();
            }
        }

        // POST: Categories/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Edit(AddEditModel model)
        {
            if (ModelState.IsValid)
            {
                Category category = await _categoryService.GetCategoryById(model.Id);
                if (category != null)
                {

                    await _categoryService.AddOrUpdate(category);
                    return RedirectToAction("Index");
                }
                else return HttpNotFound();
            }
            return View(model);
        }

        // GET: Categories/Delete/5
        public async Task<ActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Category category = await _categoryService.GetCategoryById(id);
            if (category != null)
            {
                AddEditModel model = new AddEditModel
                {
                    Id = category.Id,
                    Name = category.Name,
                };
                return View(model);
            }
            else
            {
                return HttpNotFound();
            }
        }

        // POST: Categories/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> DeleteConfirmed(int id)
        {
            Category category = await _categoryService.GetCategoryById(id);
            if (category != null)
            {
                await _categoryService.Remove(id);
                return RedirectToAction("Index");
            }
            else return HttpNotFound();
        }

        //protected override void Dispose(bool disposing)
        //{
        //    if (disposing)
        //    {
        //        db.Dispose();
        //    }
        //    base.Dispose(disposing);
        //}
    }
}
