﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Net;
using System.Web;
using System.Web.Mvc;
using CodeFirst.Models;
using Core.Services;
using Microsoft.AspNet.Identity.Owin;
using PerpetuumSoft.Knockout;
using Services;
using WebUI.Ingrastructure;
using WebUI.Models;
using WebUI.Models.Brand;
using WebUI.Models.Subcategories;
using AddEditModel = WebUI.Models.Products.AddEditModel;
using IndexModel = WebUI.Models.Products.IndexModel;

namespace WebUI.Controllers
{
    public class ProductsController : KnockoutController
    {
        private readonly IProductService _productService;
        private readonly IWarehouseService _warehouseService;
        private readonly IStockService _stockService;
        private ISubcategoryService _subcategoryService;
        private IBrandService _brandService;
        private readonly IImageService _imageService;
        //private ApplicationContext db
        //{
        //    get { return HttpContext.GetOwinContext().Get<ApplicationContext>(); }
        //}
       public ProductsController(
           IProductService productService, 
           IWarehouseService warehouseService,
            IStockService stockService,
            ISubcategoryService subcategoryService,
            IBrandService brandService,
            IImageService imageService)
        {
            _productService = productService;
            _warehouseService = warehouseService;
            _stockService = stockService;
            _brandService = brandService;
            _subcategoryService = subcategoryService;
            _imageService = imageService;
        }
        // GET: Products
        public async Task<ActionResult> Index()
        {
            IndexModel model = new IndexModel
            {
                Products = await _productService.GetAllProducts()
            };
            return View(model);
        }

        // GET: Products/Details/5
        public async Task<ActionResult> Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Product product = await _productService.GetProductById(id);
            AddEditModel productViewModel = new AddEditModel
            {
                Id = product.Id,
                Name = product.Name,
                Description = product.Description,
                BrandId = product.BrandId,
                Price = product.Price,
                SubcategoryId = product.SubcategoryId
            };
            if (product == null)
            {
                return HttpNotFound();
            }
            return View(productViewModel);
        }

        // GET: Products/Create
        public async Task<ActionResult> Create()
        {
            AddEditModel model = new AddEditModel
            {
                Subcategories = MapperService.ConvertCollection<Subcategory, SubcategoryViewModel>(await _subcategoryService.GetAllSubcategories()),
                Brands = MapperService.ConvertCollection<Brand, BrandViewModel>(await _brandService.GetAllBrands())
            }; 
            return View(model);
        }

        // POST: Products/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Create(AddEditModel model, IEnumerable<HttpPostedFileBase> uploadImage)
        {
            if (ModelState.IsValid)
            {
                //КОСТЫЛЬ Переписать все в маппере
                Product product = new Product
                { 
                    Name = model.Name,
                    Description = model.Description,
                    BrandId = model.BrandId,
                    Price = model.Price,
                    SubcategoryId = model.SubcategoryId
                };
                
                await _productService.AddOrUpdate(product);
                var warehouses = await _warehouseService.GetAllWarehouses();
                foreach (var warehouse in warehouses)
                {
                    Stock stock = new Stock
                    {
                        // как вытащить ID созданной энтити
                        WarehouseId = warehouse.Id,
                        ProductId = product.Id
                    };
                    //вот так))
                  await  _stockService.AddOrUpdate(stock);
                }
                if (uploadImage != null)
                {
                   product.DefaultImageId =  await _imageService.UploadProductImages(uploadImage, product.Id);
                }
                await _productService.AddOrUpdate(product);

                return RedirectToAction("Index");
            }

            return View(model);
        }

        // GET: Products/Edit/5
        public async Task<ActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Product product = await _productService.GetProductById(id);
            if (product != null)
            {
            //Использовать профили маппера для маппинга
                AddEditModel productViewModel = new AddEditModel
                {
                    Id = product.Id,
                    Name = product.Name,
                    Description = product.Description,
                    BrandId = product.BrandId,
                    Price = product.Price,
                    SubcategoryId = product.SubcategoryId,
                    Subcategories =  MapperService.ConvertCollection<Subcategory,SubcategoryViewModel>(await _subcategoryService.GetAllSubcategories()),
                    Brands = MapperService.ConvertCollection <Brand,BrandViewModel>(await _brandService.GetAllBrands()),
                    Images = MapperService.ConvertCollection<Image,ImageViewModel>(await _imageService.GetAllImagesByProductId(product.Id)),
                    DefaultImageId = product.DefaultImageId
                };
                return View(productViewModel);
            }
            else
            {
                return HttpNotFound();
            }
        }
        [HttpPost]
        public async Task<JsonResult> UploadImages(int productId)
        {
            List<HttpPostedFileBase> uploadedImagesPosted = new List<HttpPostedFileBase>();
            foreach (string file in Request.Files)
            {
                uploadedImagesPosted.Add(Request.Files[file]);
            }
            List<ImageViewModel> UploadedImages = MapperService.ConvertCollection<Image,ImageViewModel>(_imageService.UploadProductImagesReturnImages(uploadedImagesPosted, productId).Result);
           
            return Json(new { Success = true, UploadedImages = UploadedImages }, "applicattion/json");
        }

        // POST: Products/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        public async Task<JsonResult> Edit(AddEditModel productViewModel)
        {
            if (ModelState.IsValid)
            {
                Product product = await _productService.GetProductById(productViewModel.Id);
                if (product != null)
                {
                    //Костыль, переписать все на мапперы потом
                    product.BrandId = productViewModel.BrandId;
                    product.Name = productViewModel.Name;
                    product.Description = productViewModel.Description;
                    product.Price = productViewModel.Price;
                    product.SubcategoryId = productViewModel.SubcategoryId;
                    product.DefaultImageId = productViewModel.DefaultImageId;
                    await _productService.AddOrUpdate(product);
                    if (productViewModel.UploadedImages != null)
                    {
                        productViewModel.Images.AddRange(productViewModel.UploadedImages);
                    }
                        if (productViewModel.Images!=null)
                        //т.к. внутри происходит удаление, придется использовать фор
                    for( var i = 0;i< productViewModel.Images.Count;i++)
                    {
                        if (productViewModel.Images[i].isDeleted)
                        {
                            await _imageService.RemoveImage(productViewModel.Images[i].Id);
                                productViewModel.Images.Remove(productViewModel.Images[i]);
                            i = i-1;
                        }
                    }
                    
                        productViewModel.UploadedImages = new List<ImageViewModel>();
                        productViewModel.UploadedImagesPreview = new List<string>();

                    

                    return Json(new {Model = productViewModel,Success = true},"applicattion/json");
                }
            }
            return Json(productViewModel);
        }
       
        // GET: Products/Delete/5
        public async Task<ActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Product product = await _productService.GetProductById(id);
            if (product != null)
            {
                AddEditModel productViewModel = new AddEditModel
                {
                    Id = product.Id,
                    Name = product.Name,
                    Description = product.Description,
                    Images = MapperService.ConvertCollection<Image, ImageViewModel>(await _imageService.GetAllImagesByProductId(product.Id))
                };
                return View(productViewModel);
            }
            else
            {
                return HttpNotFound();
            }
        }

        // POST: Products/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> DeleteConfirmed(int id)
        {
            Product product = await _productService.GetProductById(id);
            if (product != null)
            {
                await _productService.Remove(id);
                return RedirectToAction("Index");
            }
            else return HttpNotFound();
        }
    }
}
