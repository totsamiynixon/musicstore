﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using CodeFirst.Models;
using Core.Services;
using WebUI.Models.Warehouses;

namespace WebUI.Controllers
{
    public class WarehouseController : Controller
    {
        // GET: Warehouse
        private readonly IWarehouseService _warehouseService;
        private readonly IStockService _stockService;
        private readonly IProductService _productService;

        public WarehouseController(IWarehouseService warehouseService, IStockService stockService, IProductService productService)
        {
            _warehouseService = warehouseService;
            _stockService = stockService;
            _productService = productService;
        }

        // GET: Warehouses
        public async Task<ActionResult> Index()
        {
            IndexModel model = new IndexModel
            {
                Warehouses = await _warehouseService.GetAllWarehouses()
            };
            return View(model);
        }

        // GET: Warehouses/Details/5
        //public async Task<ActionResult> Details(int? id)
        //{
        //    if (id == null)
        //    {
        //        return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
        //    }
        //    Warehouse warehouse = await _warehouseService.GetWarehouseById(id);
        //    if (warehouse != null)
        //    {
        //        WarehouseDetailsModel detailsModel = new WarehouseDetailsModel
        //        {
        //            Address = warehouse.Address,
        //            Stocks = await _stockService.GetAllStocksByWarehouseId(warehouse.Id)
        //        };
        //        return View(detailsModel);
        //    }
        //   else
        //    {
        //        return HttpNotFound();
        //    }
        //}

        // GET: Warehouses/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Warehouses/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Create(EditModel model)
        {
            if (ModelState.IsValid)
            {
                Warehouse warehouse = new Warehouse
                {
                   Address = model.Address
                };
              int id =  await _warehouseService.AddOrUpdate(warehouse);
                List<Product> products = await _productService.GetAllProducts();
                foreach (var product in products)
                {
                    Stock stock = new Stock
                    {
                        NumberOfProducts = 0,
                        ProductId = product.Id,
                        WarehouseId = id
                    };
                   await _stockService.AddOrUpdate(stock);
                }
                return RedirectToAction("Index");
            }

            return View(model);
        }

        // GET: Warehouses/Edit/5
        public async Task<ActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Warehouse warehouse = await _warehouseService.GetWarehouseById(id);
            if (warehouse != null)
            {
                EditModel model = new EditModel
                {
                    Id = warehouse.Id,
                    Address = warehouse.Address
                };
                return View(model);
            }
            else
            {
                return HttpNotFound();
            }
        }

        // POST: Warehouses/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Edit(EditModel model)
        {
            if (ModelState.IsValid)
            {
                Warehouse warehouse = await _warehouseService.GetWarehouseById(model.Id);
                if (warehouse != null)
                {
                    warehouse.Address = model.Address;
                    await _warehouseService.AddOrUpdate(warehouse);
                    return RedirectToAction("Index");
                }
                else return HttpNotFound();
            }
            return View(model);
        }

        // GET: Warehouses/Delete/5
        public async Task<ActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Warehouse warehouse = await _warehouseService.GetWarehouseById(id);
            if (warehouse != null)
            {
                EditModel model = new EditModel
                {
                    Id = warehouse.Id,
                   Address = warehouse.Address
                };
                return View(model);
            }
            else
            {
                return HttpNotFound();
            }
        }

        // POST: Warehouses/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> DeleteConfirmed(int id)
        {
            Warehouse warehouse = await _warehouseService.GetWarehouseById(id);
            if (warehouse != null)
            {
                await _warehouseService.Remove(id);
                return RedirectToAction("Index");
            }
            else return HttpNotFound();
        }
    }
}