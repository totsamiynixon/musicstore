﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Net;
using System.Web;
using System.Web.Mvc;
using CodeFirst.Models;
using Core.Services;
using Microsoft.AspNet.Identity.Owin;
using WebUI.Models.Couriers;

namespace WebUI.Controllers
{
    public class CouriersController : Controller
    {
        //private ApplicationContext db
        //{
        //    get { return HttpContext.GetOwinContext().Get<ApplicationContext>(); }
        //}
        private ICourierService _courierService;

        public CouriersController(ICourierService courierService)
        {
            _courierService = courierService;
        }

        // GET: Couriers
        public async Task<ActionResult> Index()
        {
            IndexModel model = new IndexModel
            {
                Couriers = await _courierService.GetAllCouriers()
            };
            return View(model);
        }

        // GET: Couriers/Details/5
        public async Task<ActionResult> CourierOrdersDetails(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Courier courier = await _courierService.GetCourierWithOrders(id.Value);
            if (courier != null)
            {
                AddEditModel model = new AddEditModel
                { 
                    Id = courier.Id,
                    Name = courier.Name,
                    Address = courier.Address,
                    Salary = courier.Salary,
                    Email = courier.Email,
                    Orders = courier.Orders
                    
                };
                return View(model);
            }
            else
            {
                return HttpNotFound();
            }
        }

        // GET: Couriers/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Couriers/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Create(AddEditModel model)
        {
            if (ModelState.IsValid)
            {
                Courier courier = new Courier
                {
                    Email = model.Email,
                    Name = model.Name,
                    Address = model.Address,
                    Salary = 0
                    //подумать по поводу зарплаты
                };
                await _courierService.AddOrUpdate(courier);
                return RedirectToAction("Index");
            }

            return View(model);
        }

        // GET: Couriers/Edit/5
        public async Task<ActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Courier courier = await _courierService.GetCourierById(id.Value);
            if (courier != null)
            {
              AddEditModel model = new AddEditModel
                {
                    Id = courier.Id,
                    Name = courier.Name,
                    Address = courier.Address,
                    Email = courier.Email,
                    Salary = courier.Salary       
                };
                return View(model);
            }
            else
            {
                return HttpNotFound();
            }
        }

        // POST: Couriers/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Edit(AddEditModel model)
        {
            if (ModelState.IsValid)
            {
                Courier courier = await _courierService.GetCourierById(model.Id);
                if (courier != null)
                {

                    await _courierService.AddOrUpdate(courier);
                    return RedirectToAction("Index");
                }
                else return HttpNotFound();
            }
            return View(model);
        }

        // GET: Couriers/Delete/5
        public async Task<ActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
           Courier courier = await _courierService.GetCourierById(id.Value);
            if (courier != null)
            {
                AddEditModel model = new AddEditModel
                {
                    Id = courier.Id,
                    Name = courier.Name,
                    Address = courier.Address,
                    Email = courier.Email,
                    Salary = courier.Salary
                };
                return View(model);
            }
            else
            {
                return HttpNotFound();
            }
        }

        // POST: Couriers/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> DeleteConfirmed(int id)
        {
            Courier courier = await _courierService.GetCourierById(id);
            if (courier != null)
            {
                await _courierService.Remove(id);
                return RedirectToAction("Index");
            }
            else return HttpNotFound();
        }

        //protected override void Dispose(bool disposing)
        //{
        //    if (disposing)
        //    {
        //        db.Dispose();
        //    }
        //    base.Dispose(disposing);
        //}
    }
}
