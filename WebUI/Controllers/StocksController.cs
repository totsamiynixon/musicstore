﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Net;
using System.Web;
using System.Web.Mvc;
using CodeFirst.Models;
using Core.Services;
using Microsoft.AspNet.Identity.Owin;
using WebUI.Models.Stocks;

namespace WebUI.Controllers
{
    public class StocksController : Controller
    {
        private readonly IStockService _stockService;
        private readonly IWarehouseService _warehouseService;

        public StocksController(IStockService stockService,  IWarehouseService warehouseService)
        {
            _stockService = stockService;
            _warehouseService = warehouseService;
        }

        // GET: Stocks
        public async Task<ActionResult> Index()
        {
            IndexModel model = new IndexModel
            {
                Stocks = await _stockService.GetAllStocksWithProducts()
            };
            return View(model);
        }
        [HttpGet]
        public async Task<ActionResult> AddEdit(int? productId)
        {
            if (productId == null) return HttpNotFound();
            var stocks = await _stockService.GetAllStocksByProductId(productId);
            var warehouses = await _warehouseService.GetAllWarehouses();
            AddEditModel model = new AddEditModel();
            if (stocks == null || stocks.Count == 0)
            {
                foreach (var warehouse in warehouses)
                {
                    StockHolder stockHolder = new StockHolder
                    {
                        ProductId = productId.Value,
                        CurrentNumberOfProducts = 0,
                        WarehouseId = warehouse.Id,
                        WarehouseAddress = warehouse.Address
                    };
                    model.StockHolders.Add(stockHolder);
                }
            }
            else
            {
                foreach (var stock in stocks)
                {
                    var warehouse = warehouses.FirstOrDefault(w => w.Id == stock.WarehouseId);
                    StockHolder stockHolder = new StockHolder
                    {
                        ProductId = stock.ProductId,
                        CurrentNumberOfProducts = stock.NumberOfProducts,
                        WarehouseId = stock.WarehouseId,
                        Id = stock.Id,
                        WarehouseAddress = warehouse.Address
                    };
                    model.StockHolders.Add(stockHolder);
                }
            }
            return View(model);

        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> AddEdit(AddEditModel model)
        {
            if (ModelState.IsValid)
            {
                foreach (var stockHolder in model.StockHolders)
                {
                    Stock stock = new Stock
                    {
                        Id = stockHolder.Id,
                        NumberOfProducts = stockHolder.CurrentNumberOfProducts + stockHolder.CountToAdd,
                        ProductId = stockHolder.ProductId,
                        WarehouseId = stockHolder.WarehouseId
                    };
                    await _stockService.AddOrUpdate(stock);
                }
                return RedirectToAction("Index", "Products");
            }
            return View(model);

        }
      
}
}
