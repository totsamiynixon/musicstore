﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using CodeFirst.Enums;
using CodeFirst.Models;
using Core.Services;
using Newtonsoft.Json;
using WebUI.Ingrastructure;
using WebUI.Models.API;
using WebUI.Models.API.Brands;
using WebUI.Models.API.Categories;
using WebUI.Models.API.Customer;
using WebUI.Models.API.Products;
using WebUI.Models.API.Sucategory;

namespace WebUI.Controllers
{
    public class CustomerController : Controller
    {
        private ICategoryService _categoryService;
        private IProductService _productService;
        private IBrandService _brandService;
        private ISubcategoryService _subcategoryService;

        public CustomerController(
            ICategoryService categoryService,
            IProductService productService,
            IBrandService brandService,
            ISubcategoryService subcategoryService
            )
        {
            _categoryService = categoryService;
            _productService = productService;
            _brandService = brandService;
            _subcategoryService = subcategoryService;
        }
        // GET: Customer
        public ActionResult Index()
        {

            var categories = _categoryService.GetAllCategories().Result;
            
            return View(categories);
        }
        [HttpPost]
        public async Task<ActionResult> GetAllProducts(string model)
        {
            var viewModel = JsonConvert.DeserializeObject<ApiCustomerViewModel>(model);
            //model = await CategoryChanges(model);
            viewModel.Products = MapperService.ConvertCollection<Product, ApiProduct>(_productService.GetAllProducts().Result);
            viewModel = PaginationChanges(viewModel);
            var jsonModel = JsonConvert.SerializeObject(viewModel.Products);
            return Json(jsonModel);
        }

        [HttpPost]
        public ActionResult Init()
        {
            List<ApiBrand> brands = MapperService.ConvertCollection<Brand, ApiBrand>(_brandService.GetAllBrands().Result);
            List<ApiCategory> categories = MapperService.ConvertCollection<Category, ApiCategory>(_categoryService.GetAllCategories().Result);
            var products = MapperService.ConvertCollection<Product, ApiProduct>(_productService.GetAllProducts().Result.Where(p=>p.Subcategory.CategoryId == categories.First().Id).ToList());
            ApiCustomerViewModel model = CustomerInit(brands, categories, products, 0);
            var jsonModel = JsonConvert.SerializeObject(model);
            return Json(jsonModel);
        }

        public async Task<ActionResult> Catalog(string subcategoryName)
        {
            var subcategory =
                _subcategoryService.GetAllSubcategories()
                    .Result.Find(m => m.Name.ToLower() == subcategoryName.ToLower());
            if (subcategory != null)
            {
                var productsEntity = _productService.GetAllProductsBySubcategoryId(subcategory.Id);
                var categoriesEntity = _categoryService.GetAllCategoriesWithSubcategories();
                await productsEntity;
                var products = MapperService.ConvertCollection<Product,ApiProduct>(productsEntity.Result);
                await categoriesEntity;
                List<Brand> brandsEntity = new List<Brand>();
                foreach (var product in productsEntity.Result)
                {
                if(!brandsEntity.Exists(p=>p.Id == product.BrandId))
                   brandsEntity.Add(product.Brand);
                }
                brandsEntity = brandsEntity.Distinct().ToList();
                var brands = MapperService.ConvertCollection<Brand, ApiBrand>(brandsEntity);


                var categories=  MapperService.ConvertCollection<Category,ApiCategory>(categoriesEntity.Result);

                ApiCustomerViewModel model = CustomerInit(brands, categories, products,subcategory.Id);

                return  View("Index",model);
            }
            else return HttpNotFound();
        }






        private ApiCustomerViewModel CustomerInit(List<ApiBrand> brands, List<ApiCategory> categories, List<ApiProduct> products, int subcategoryId)
        {
            ApiSidebar sidebar = new ApiSidebar
            {
                Brands = brands,
                StartPrice = 0,
                FinishPrice = 10000,
                Categories = categories
            };
            ApiPagination pagination = new ApiPagination
            {
                ItemsPerPage = 3,
                CurrentPage = 1,
                Pages = new List<int>(),
                CurrentSortType = (int)ProductsSortType.NameAscending
            };
            int numberOfPages = products.Count == pagination.ItemsPerPage
                ? 1
                : products.Count / pagination.ItemsPerPage + 1;
            for (int i = 0; i < numberOfPages; i++)
            {
                pagination.Pages.Add(i);
            }
            products.OrderBy(m => m.Name);
            products = products.GetRange(0, pagination.ItemsPerPage>products.Count ? products.Count: pagination.ItemsPerPage);
           
            ApiCustomerViewModel model = new ApiCustomerViewModel
            {

                Products = products,
                Sidebar = sidebar,
                Pagination = pagination,
                CurrentCategoryId = categories.First().Id,
                CurrentSubcategoryId = 0,
                //для расширения ( см внутри вьюхи для этого метода)
            };
            return model;
        }
        private ApiCustomerViewModel PaginationChanges(ApiCustomerViewModel model)
        {
            var products = model.Products;
            int numberOfPages = products.Count == model.Pagination.ItemsPerPage
                ? 1
                : products.Count / model.Pagination.ItemsPerPage + 1;
            model.Pagination.Pages.Clear();
            for (int i = 0; i < numberOfPages; i++)
            {
                model.Pagination.Pages.Add(i);
            }
            int index;
            if (model.Pagination.CurrentPage == 1)
            {
                index = 0;

            }
            else
            {
                index = model.Pagination.ItemsPerPage * (model.Pagination.CurrentPage - 1) - 1;
            }
            products = products.GetRange(index,
                model.Pagination.ItemsPerPage * model.Pagination.CurrentPage > products.Count ?
                products.Count - (model.Pagination.ItemsPerPage * (model.Pagination.CurrentPage - 1)) : model.Pagination.ItemsPerPage);
            model.Products = products;

            return model;
        }
    }
}