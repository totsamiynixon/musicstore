﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Net;
using System.Web;
using System.Web.Mvc;
using CodeFirst.Models;
using Core.Services;
using Microsoft.AspNet.Identity.Owin;
using WebUI.Models.Subcategories;

namespace WebUI.Controllers
{
    public class SubcategoriesController : Controller
    {
        //private ApplicationContext db
        //{
        //    get { return HttpContext.GetOwinContext().Get<ApplicationContext>(); }
        //}
        private readonly ISubcategoryService _subcategoryService;
        private readonly ICategoryService _categoryService;

        public SubcategoriesController(ISubcategoryService subcategoryService, ICategoryService categoryService)
        {
            _subcategoryService = subcategoryService;
            _categoryService = categoryService;
        }

        // GET: Subcategories
        public async Task<ActionResult> Index()
        {
            IndexModel model = new IndexModel
            {
                Subcategorieses = await _subcategoryService.GetAllSubcategories()
            };
            return View(model);
        }

        public async Task<ActionResult> Create()
        {
            AddEditModel model = new AddEditModel
            {
                Categories =  await _categoryService.GetAllCategories()
            };
            return View(model);
        }

        // POST: Categories/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Create(AddEditModel model)
        {
            if (ModelState.IsValid)
            {
                Subcategory subcategory = new Subcategory
                {
                    Name = model.Name,
                    CategoryId = model.CategoryId   
                };
                await _subcategoryService.AddOrUpdate(subcategory);
                return RedirectToAction("Index");
            }
            return View(model);
        }

        // GET: Categories/Edit/5
        public async Task<ActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Subcategory subcategory = await _subcategoryService.GetSubcategoryById(id);
            if (subcategory != null)
            {
                AddEditModel model = new AddEditModel
                {
                    Id = subcategory.Id,
                    Name = subcategory.Name,
                    CategoryId = subcategory.CategoryId,
                    Categories = await _categoryService.GetAllCategories()
                };
                return View(model);
            }
            else
            {
                return HttpNotFound();
            }
        }

        // POST: Categories/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Edit(AddEditModel model)
        {
            if (ModelState.IsValid)
            {
                Subcategory subcategory = new Subcategory
                {
                    Id = model.Id,
                    CategoryId = model.CategoryId,
                    Name = model.Name,
                };
                await _subcategoryService.AddOrUpdate(subcategory);
                return RedirectToAction("Index");
            }
            return View(model);
        }
        public async Task<ActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Subcategory subcategory = await _subcategoryService.GetSubcategoryById(id);
            if (subcategory == null)
            {
                return HttpNotFound();
            }
            return View(subcategory);
        }

        // POST: Subcategories/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> DeleteConfirmed(int id)
        {
           await _subcategoryService.Remove(id);
            return RedirectToAction("Index");
        }

        //protected override void Dispose(bool disposing)
        //{
        //    if (disposing)
        //    {
        //        db.Dispose();
        //    }
        //    base.Dispose(disposing);
        //}
    }
}
