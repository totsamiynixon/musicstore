﻿//using System;
//using System.Collections.Generic;
//using System.Configuration;
//using System.Linq;
//using System.Web;
//using System.Web.Mvc;
//using CodeFirst.Abstract;
//using CodeFirst.Concrete;
//using Ninject;

//namespace WebUI.Infrastructure
//{
//    public class NinjectDependencyResolver : IDependencyResolver
//    {
//        private IKernel kernel;

//        public NinjectDependencyResolver(IKernel kernelParam)
//        {
//            kernel = kernelParam;
//            AddBindings();
//        }

//        public object GetService(Type serviceType)
//        {
//            return kernel.TryGet(serviceType);
//        }

//        public IEnumerable<object> GetServices(Type serviceType)
//        {
//            return kernel.GetAll(serviceType);
//        }

//        private void AddBindings()
//        {
//            kernel.Bind<IBasketRepository>().To<EFBasketRepository>();
//            kernel.Bind<IBrandRepository>().To<EFBrandRepository>();
//            kernel.Bind<ICategoryRepository>().To<EFCategoryRepository>();
//            kernel.Bind<ICourierRepository>().To<EFCourierRepository>();
//            kernel.Bind<IOrderRepository>().To<EFOrderRepository>();
//            kernel.Bind<IProductRepository>().To<EFProductRepository>();
//            kernel.Bind<IStockRepository>().To<EFStockRepository>();
//            kernel.Bind<ISubcategoryRepository>().To<EFSubcategoryRepository>();

//        }
//    }
//}