﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using CateoryEntity = CodeFirst.Models.Category;

namespace WebUI.Models.Categories
{
    public class IndexModel
    {
        public List<CateoryEntity> Categories { get; set; }
    }
}