﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using CodeFirst.Models;

namespace WebUI.Models.Categories
{
    public class AddEditModel
    {
        public int Id { get; set; }
        public string Name { get; set; }
    }
}