﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace WebUI.Models
{
    public class ImageViewModel
    {
      
        public int Id { get; set; }
        [Required]
        [StringLength(512)]
        public string RelatedUrl { get; set; }

        [Required]
        public int ProductId { get; set; }

        public bool isDeleted { get; set; } = false;

    }
}