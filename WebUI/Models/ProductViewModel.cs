﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using CodeFirst.Models;

namespace WebUI.Models
{
    public class ProductViewModel
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public double Price { get; set; }
        public string Description { get; set; }
        public List<Image> Images { get; set; }

        public List<CodeFirst.Models.Brand> Brands { get; set; }
        public List<Subcategory> Subcategories { get; set; }
        [Required]
        public int BrandId { get; set; }
        [Required]
        public int SubcategoryId { get; set; }


    }
}