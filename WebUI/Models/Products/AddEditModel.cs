﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Net;
using System.Web;
using CodeFirst.Models;
using Microsoft.AspNet.Identity;
using FluentValidation;
using WebUI.Models.Brand;
using WebUI.Models.Subcategories;

namespace WebUI.Models.Products
{
    public class AddEditModel
    {
        public int Id { get; set; }
        [Required]
        public string Name { get; set; }
        [Required]
        public double Price { get; set; }
        [Required]
        public string Description { get; set; }
        [Required]
        public int BrandId { get; set; }
        [Required]
        public int SubcategoryId { get; set; }
        [Required]
        public int DefaultImageId { get; set; }
        public  List<Stock> Stocks { get; set; }
        public  List<ImageViewModel> Images { get; set; }
        public List<BrandViewModel> Brands { get; set; }
        public List<SubcategoryViewModel> Subcategories { get; set; }
        public List<string> UploadedImagesPreview { get; set; }
        public List<ImageViewModel> UploadedImages { get; set; }
        


        

    }
}