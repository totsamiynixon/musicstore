﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using CodeFirst.Models;

namespace WebUI.Models.Products
{
    public class IndexModel
    {
        public List<Product> Products { get; set; }
    }
}