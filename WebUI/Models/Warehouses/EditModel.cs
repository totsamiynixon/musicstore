﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using CodeFirst.Models;

namespace WebUI.Models.Warehouses
{
    public class EditModel
    {
        public int Id { get; set; }
        public string Address { get; set; }
    }
}