﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using CodeFirst.Models;

namespace WebUI.Models.Warehouses
{
    public class IndexModel
    {
        public List<Warehouse> Warehouses { get; set; }
    }
}