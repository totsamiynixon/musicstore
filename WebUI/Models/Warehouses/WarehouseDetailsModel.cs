﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using CodeFirst.Models;

namespace WebUI.Models.Warehouses
{
    public class WarehouseDetailsModel
    {
        public string Address { get; set; }
        public List<Stock> Stocks { get; set; }
    }
}