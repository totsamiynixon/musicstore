﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using CodeFirst.Models;

namespace WebUI.Models.Couriers
{
    public class IndexModel
    {
        public List<Courier> Couriers { get; set; }
    }
}