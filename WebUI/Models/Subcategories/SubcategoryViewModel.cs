﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using CodeFirst.Models;

namespace WebUI.Models.Subcategories
{
    public class SubcategoryViewModel
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public int CategoryId { get; set; }


    }
}