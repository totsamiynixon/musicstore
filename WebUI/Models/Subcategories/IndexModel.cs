﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using CodeFirst.Models;

namespace WebUI.Models.Subcategories
{
    public class IndexModel
    {
        public List<Subcategory> Subcategorieses { get; set; }
    }
}