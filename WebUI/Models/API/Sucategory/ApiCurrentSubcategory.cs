﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Newtonsoft.Json;

namespace WebUI.Models.API.Sucategory
{
    public class ApiCurrentSubcategory
    {
        [JsonProperty("current_subcategory_id")]
        public int CurrentSubcategoryId { get; set; }
        [JsonProperty("is_changed")]
        public bool IsChanged { get; set; } = false;
    }
}