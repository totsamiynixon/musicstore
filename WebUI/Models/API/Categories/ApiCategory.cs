﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Newtonsoft.Json;
using WebUI.Models.API.Sucategory;

namespace WebUI.Models.API.Categories
{
    public class ApiCategory
    {
        [JsonProperty("id")]
        public int Id { get; set; }
        [JsonProperty("name")]
        public string Name { get; set; }
        [JsonProperty("subcategories")]
        public  List<ApiSubcategory> Subcategories { get; set; }
    }
}