﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using Newtonsoft.Json;

namespace WebUI.Models.API.Images
{
    public class ApiImage
    {
        [JsonProperty("id")]
        public int Id { get; set; }
        [JsonProperty("related_url")]
        public string RelatedUrl { get; set; }
        [JsonProperty("product_id")]
        public int ProductId { get; set; }
    }
}