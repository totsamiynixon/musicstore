﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using CodeFirst.Models;
using Newtonsoft.Json;
using WebUI.Models.API.Products;
using WebUI.Models.API.Sucategory;

namespace WebUI.Models.API.Customer
{
    [JsonObject]
    public class ApiCustomerViewModel
    {
        [JsonProperty("sidebar")]
        public ApiSidebar Sidebar { get; set; }
        [JsonProperty("products")]
        public List<ApiProduct> Products { get; set; }
        [JsonProperty("current_subcategory_id")]
        public int CurrentSubcategoryId { get; set; }
        [JsonProperty("pagination")]
        public ApiPagination Pagination { get; set; }
        [JsonProperty("current_category_id")]
        public int CurrentCategoryId { get; set; }
        

    }
}