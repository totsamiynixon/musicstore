﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Newtonsoft.Json;
using WebUI.Models.API.Brands;
using WebUI.Models.API.Categories;

namespace WebUI.Models.API.Customer
{
    public class ApiSidebar
    {
        [JsonProperty("brands")]
        public List<ApiBrand> Brands { get; set; }
        [JsonProperty("start_price")]
        public int StartPrice { get; set; }
        [JsonProperty("finish_price")]
        public int FinishPrice { get; set; }
        [JsonProperty("categories")]
        public List<ApiCategory> Categories { get; set; }


    }
}