﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Newtonsoft.Json;

namespace WebUI.Models.API
{
    [JsonObject]
    public class ApiPagination
    {
        [JsonProperty("current_page")]
        public int CurrentPage { get; set; }
        [JsonProperty("items_per_page")]
        public int ItemsPerPage { get; set; }
        [JsonProperty("pages")]
        public List<int> Pages { get; set; }
        [JsonProperty("current_sort_type")]
        public int CurrentSortType { get; set; }
    }
}