﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Newtonsoft.Json;
using WebUI.Models.API.Images;

namespace WebUI.Models.API.Products
{
    public class ApiProduct
    {
        [JsonProperty("id")]
        public int Id { get; set; }
        [JsonProperty("name")]
        public string Name { get; set; }
        [JsonProperty("price")]
        public double Price { get; set; }
        [JsonProperty("description")]
        public string Description { get; set; }
        [JsonProperty("default_image_id")]
        public int DefaultImageId { get; set; }
        [JsonProperty("images")]
        public List<ApiImage> Images { get; set; }
    }
}