﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using CodeFirst.Models;

namespace WebUI.Models
{
   public class RegisterModel
    { 
        [Required]
        public string UserName { get; set; }
        [Required]
        public string Email { get; set; }   

        [DataType(DataType.PhoneNumber)]
        public string PhoneNumber { get; set; }

        [Required]
        [DataType(DataType.Password)]
        public string Password { get; set; }


        [Required]
        [Compare("Password", ErrorMessage = "Пароли не совпадают")]
        [DataType(DataType.Password)]
        public string PasswordConfirm { get; set; }
        //[Required]
        //public ApplicationRole ApplicationRole { get; set; }
        public IEnumerable<ApplicationRole> ApplicationRoles { get; set; }
        public string RoleId { get; set; }
        //public IEnumerable<ApplicationRole> ApplicationRoles { get; set; }
    }
}