﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using CodeFirst.Models;
using BrandEntity = CodeFirst.Models.Brand;

namespace WebUI.Models.Brand
{
    public class IndexModel
    {
       public List<BrandEntity> Brands { get; set; }
    }
}