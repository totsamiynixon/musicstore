﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebUI.Models.Stocks
{
    public class StockHolder
    {
        public int Id { get; set; }
        public int ProductId { get; set; }
        public int WarehouseId { get; set; }
        public string WarehouseAddress { get; set; }
        public int CurrentNumberOfProducts { get; set; }
        public int CountToAdd { get; set; }

    }
}