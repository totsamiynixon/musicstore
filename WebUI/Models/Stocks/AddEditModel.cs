﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using CodeFirst.Models;

namespace WebUI.Models.Stocks
{
    public class AddEditModel
    {
        public AddEditModel()
        {
            StockHolders = new List<StockHolder>();
        }
        public int Id { get; set; }
        public List<StockHolder> StockHolders { get; set; }
    }
}