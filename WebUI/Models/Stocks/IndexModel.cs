﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using CodeFirst.Models;

namespace WebUI.Models.Stocks
{
    public class IndexModel
    {
        public List<Stock> Stocks { get; set; }
    }
}