﻿using System.Collections.Generic;
using System.Collections.Specialized;
using System.Data.Entity.SqlServer.Utilities;
using System.Data.Entity.Utilities;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using System.Web.Http.Results;
using System.Web.Mvc;
using AutoMapper;
using CodeFirst.Enums;
using CodeFirst.Models;
using Core.Services;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using WebUI.Models.Products;
using WebUI.Ingrastructure;
using WebUI.Models;
using WebUI.Models.API;
using WebUI.Models.API.Brands;
using WebUI.Models.API.Categories;
using WebUI.Models.API.Customer;
using WebUI.Models.API.Products;
using WebUI.Models.Brand;
using WebUI.Models.Subcategories;

namespace WebUI.Controllers
{
    public class ProductWebController : ApiController
    {
        private ICategoryService _categoryService;
        private IBrandService _brandService;
        private readonly IProductService _productSercice;
        private readonly ISubcategoryService _subcategoryService;
        private readonly IImageService _imageService;
       public ProductWebController(
           IProductService productService, 
           ISubcategoryService subcategoryService, 
           IImageService imageService,
           ICategoryService categoryService,
            IBrandService brandService      
           )
        {
            _productSercice = productService;
            _subcategoryService = subcategoryService;
            _imageService = imageService;
            _categoryService = categoryService;
            _brandService = brandService;
        }

        //[System.Web.Http.HttpPost]
        //public async Task<ApiCustomerViewModel> GetAllProducts(JObject model)
        //{
            
        //    var viewModel = JsonConvert.DeserializeObject<ApiCustomerViewModel>(model.ToString());
        //    var abc = new ApiCustomerViewModel();
        //    //model = await CategoryChanges(model);
        //    abc = PaginationChanges(abc);
        //    return abc;
        //}





        public JsonResult<string> PaginationChanges(JObject jsonModel)
        {
            var model = JsonConvert.DeserializeObject<ApiCustomerViewModel>(jsonModel.ToString());
            List<Product> dbProducts =new List<Product>();
            if (model.CurrentSubcategoryId != 0)
            {
                dbProducts = GetProductsForEachBrandBySubcategory(model);
            }
            else
            {
               dbProducts =  GetProductsForEachBrandByCategory(model);
            }
            var sortedProducts = SortProductsBy(model.Pagination,dbProducts);
            SetModelProducts(model, sortedProducts);
            var jsonResult = JsonConvert.SerializeObject(model.Products);
            return Json(jsonResult);
        }



        public async Task<JsonResult<string>> CategoryChanges(JObject jsonModel)
        {
            var model = JsonConvert.DeserializeObject<ApiCustomerViewModel>(jsonModel.ToString());
            List<Product> dbProducts = new List<Product>();
            List<Brand> dbBrands = new List<Brand>();
           
               
             dbProducts.AddRange(_productSercice.FindProductsBy(p => p.Subcategory.CategoryId == model.CurrentCategoryId &
                                                                  p.Price >= model.Sidebar.StartPrice &
                                                                  p.Price <= model.Sidebar.FinishPrice).Result);
            dbBrands = await _brandService.GetAllBrands();
            dbBrands = dbBrands.Where(m => m.Products.FindAll(p => p.Subcategory.CategoryId == model.CurrentCategoryId).Count > 0).ToList();

            SetDefaultPagination(model,dbProducts);
            var sortedProducts = SortProductsBy(model.Pagination, dbProducts);
            SetModelProducts(model, sortedProducts);
            model.Sidebar.Brands = MapperService.ConvertCollection<Brand, ApiBrand>(dbBrands);
            var jsonResult = JsonConvert.SerializeObject(model);

            return Json(jsonResult);
        }


        public async Task<JsonResult<string>> SubcategoryChanges(JObject jsonModel)
        {
            var model = JsonConvert.DeserializeObject<ApiCustomerViewModel>(jsonModel.ToString());
            List<Product> dbProducts = new List<Product>();
            List<Brand> dbBrands = new List<Brand>();
            Task<List<Brand>> dbBrandsTask;


            dbProducts.AddRange(_productSercice.FindProductsBy(p => p.SubcategoryId == model.CurrentSubcategoryId &
                                                                 p.Price >= model.Sidebar.StartPrice &
                                                                 p.Price <= model.Sidebar.FinishPrice).Result);
            dbBrandsTask = _brandService.GetAllBrands();

            SetDefaultPagination(model,dbProducts);
            var sortedProducts = SortProductsBy(model.Pagination, dbProducts);
            SetModelProducts(model, sortedProducts);
            dbBrands = await dbBrandsTask;
            dbBrands = dbBrands.Where(m => m.Products.FindAll(p => p.SubcategoryId == model.CurrentSubcategoryId).Count > 0).ToList();
            model.Sidebar.Brands = MapperService.ConvertCollection<Brand, ApiBrand>(dbBrands);
            var jsonResult = JsonConvert.SerializeObject(model);

            return Json(jsonResult);
        }

        public async Task<JsonResult<string>> SidebarChanges(JObject jsonModel)
        {
            var model = JsonConvert.DeserializeObject<ApiCustomerViewModel>(jsonModel.ToString());
            List<Product> dbProducts = new List<Product>();

            if (model.CurrentSubcategoryId != 0)
            {
              dbProducts = GetProductsForEachBrandBySubcategory(model);
            }
            else
            {
                dbProducts = GetProductsForEachBrandByCategory(model);
            }
            SetDefaultPagination(model, dbProducts);
            SortProductsBy(model.Pagination, dbProducts);
            SetModelProducts(model, dbProducts);
            var jsonResult = JsonConvert.SerializeObject(model);

            return Json(jsonResult);
        }


        private List<Product> GetProductsForEachBrandByCategory(ApiCustomerViewModel model)
        {
            List<Product> dbProducts = new List<Product>();
            if (model.Sidebar.Brands.FirstOrDefault(b => b.isChosen) != null)
            {
                foreach (var brand in model.Sidebar.Brands)
                {
                    if (brand.isChosen)
                    {
                        dbProducts.AddRange(
                            _productSercice.FindProductsBy(
                                p => p.Subcategory.CategoryId == model.CurrentCategoryId &
                                     p.BrandId == brand.Id &
                                     p.Price >= model.Sidebar.StartPrice &
                                     p.Price <= model.Sidebar.FinishPrice).Result);
                    }
                }
            }
            else
                dbProducts = _productSercice.FindProductsBy(p => p.Subcategory.CategoryId == model.CurrentCategoryId &
                                                                 p.Price >= model.Sidebar.StartPrice &
                                                                 p.Price <= model.Sidebar.FinishPrice).Result;
            return dbProducts;

        }

        private List<Product> GetProductsForEachBrandBySubcategory(ApiCustomerViewModel model)
        {
            List<Product> dbProducts = new List<Product>();
            if (model.Sidebar.Brands.FirstOrDefault(b => b.isChosen) != null)
            {
                foreach (var brand in model.Sidebar.Brands)
                {
                    if (brand.isChosen)
                    {
                        dbProducts.AddRange(
                            _productSercice.FindProductsBy(p => p.SubcategoryId == model.CurrentSubcategoryId &
                                                                p.BrandId == brand.Id &
                                                                p.Price >= model.Sidebar.StartPrice &
                                                                p.Price <= model.Sidebar.FinishPrice).Result);
                    }
                }
            }
            else
                dbProducts = _productSercice.FindProductsBy(p => p.SubcategoryId == model.CurrentSubcategoryId &
                                                                 p.Price >= model.Sidebar.StartPrice &
                                                                 p.Price <= model.Sidebar.FinishPrice).Result;
            return dbProducts;

        }

        private void SetDefaultPagination(ApiCustomerViewModel model, List<Product> dbProducts)
        {
            model.Pagination.CurrentPage = 1;
            int numberOfPages = dbProducts.Count == model.Pagination.ItemsPerPage
               ? 1
               : dbProducts.Count / model.Pagination.ItemsPerPage + 1;
            model.Pagination.Pages.Clear();
            for (int i = 0; i < numberOfPages; i++)
            {
                model.Pagination.Pages.Add(i);
            }
        }

        private void SetModelProducts(ApiCustomerViewModel model, List<Product> dbProducts)
        {
            int index;
            if (model.Pagination.CurrentPage == 1)
            {
                index = 0;

            }
            else
            {
                index = model.Pagination.ItemsPerPage * (model.Pagination.CurrentPage - 1) - 1;
            }
            dbProducts = dbProducts.GetRange(index == 0 ? 0 : index + 1,
                model.Pagination.ItemsPerPage * model.Pagination.CurrentPage > dbProducts.Count ?
                dbProducts.Count - (model.Pagination.ItemsPerPage * (model.Pagination.CurrentPage - 1)) : model.Pagination.ItemsPerPage);
            model.Products = MapperService.ConvertCollection<Product, ApiProduct>(dbProducts);
        }

        private List<Product> SortProductsBy(ApiPagination pagination, List<Product> dbProducts)
        {
            List<Product> sortedProducts = dbProducts;
            switch (pagination.CurrentSortType)
            {
                case (int)ProductsSortType.NameAscending :
                    sortedProducts = dbProducts.OrderBy(m => m.Name).ToList();
                    break;
                case (int)ProductsSortType.NameDescending:
                    sortedProducts = dbProducts.OrderByDescending(m => m.Name).ToList();
                    break;
                case (int)ProductsSortType.PriceAscending:
                    sortedProducts = dbProducts.OrderBy(m => m.Price).ToList();
                    break;
                case (int)ProductsSortType.PriceDescending:
                    sortedProducts = dbProducts.OrderByDescending(m => m.Price).ToList();
                    break;
                default:
                    break;

            }
            return sortedProducts;
        }
    }
}
