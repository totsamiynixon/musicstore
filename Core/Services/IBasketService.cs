﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CodeFirst.Models;

namespace Core.Services
{
    public interface IBasketService
    {
        Task<List<Basket>> GetAllBaskets();
        Task<Basket> GetBasketById(int? id);

        Task<int> AddOrUpdate(Basket basket);

        Task Remove(int id);
        Task Remove(List<int> ids);
    }
}
