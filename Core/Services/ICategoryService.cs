﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CodeFirst.Models;

namespace Core.Services
{
    public interface ICategoryService
    {
        Task<List<Category>> GetAllCategories();
        Task<List<Category>> GetAllCategoriesWithSubcategories();
        Task<Category> GetCategoryById(int? id);

        Task<int> AddOrUpdate(Category category);

        Task Remove(int id);
        Task Remove(List<int> ids);
    }
}
