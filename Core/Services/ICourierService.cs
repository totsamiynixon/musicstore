﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using CodeFirst.Models;

namespace Core.Services
{
    public interface ICourierService
    {
        Task<List<Courier>> GetAllCouriers();
        Task<Courier> GetCourierById(int? id);
        Task<List<Courier>> GetAllCouriersWithOrders();
        Task<Courier> GetCourierWithOrders(int id);
        Task<int> AddOrUpdate(Courier courier);

        Task Remove(int id);
        Task Remove(List<int> ids);
    }
}
