﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CodeFirst.Models;

namespace Core.Services
{
    public interface IOrderService
    {
        Task<List<Order>> GetAllOrders();
        Task<Order> GetOrderById(int? id);

        Task<int> AddOrUpdate(Order order);

        Task Remove(int id);
        Task Remove(List<int> ids);
    }
}
