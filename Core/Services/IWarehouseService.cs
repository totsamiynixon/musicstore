﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CodeFirst.Models;

namespace Core.Services
{
    public interface IWarehouseService
    {
        Task<List<Warehouse>> GetAllWarehouses();
        Task<Warehouse> GetWarehouseById(int? id);

        Task<int> AddOrUpdate(Warehouse warehouses);

        Task Remove(int id);
        Task Remove(List<int> ids);
    }
}
