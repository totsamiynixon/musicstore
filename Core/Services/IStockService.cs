﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CodeFirst.Models;

namespace Core.Services
{
    public interface IStockService
    {
        Task<List<Stock>> GetAllStocks();
        Task<Stock> GetStockById(int? id);

        Task<int> AddOrUpdate(Stock stock);
        Task<List<Stock>> GetAllStocksByWarehouseId(int? warehouseId);
        Task<List<Stock>> GetAllStocksWithProducts();
        Task<List<Stock>> GetAllStocksByProductId(int? productId);

        Task Remove(int id);
        Task Remove(List<int> ids);
    }
}
