﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using CodeFirst.Models;

namespace Core.Services
{
    public interface IBrandService
    {
        Task<List<Brand>> GetAllBrands();
        Task<List<Brand>> FindBrandsBy(Expression<Func<Brand, bool>> predicate);
        Task<List<Brand>> GetBrandsByCategoryId(int categoryId);
        Task<List<Brand>> GetBrandsBySubcategoryId(int subcategoryId);
        Task<Brand> GetBrandById(int? id);

        Task<int> AddOrUpdate(Brand brand);

        Task Remove(int id);
        Task Remove(List<int> ids);
    }
}
