﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using CodeFirst.Models;

namespace Core.Services
{
    public interface IImageService
    {
       Task<int> UploadProductImage(HttpPostedFileBase hpf, int productId);
        Task<int> UploadProductImages(IEnumerable<HttpPostedFileBase> hpfs, int productId);
        Task<List<Image>> UploadProductImagesReturnImages(IEnumerable<HttpPostedFileBase> hpfs, int productId);
        Task<Image> GetImageById(int imageId);
        Task<List<Image>> GetAllImagesByProductId(int productId);
        Task RemoveAllImagesWithProductId(int productId);
        Task RemoveImage(int id);
    }
}
